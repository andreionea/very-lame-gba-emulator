# http://stackoverflow.com/a/455390 => inspiration for progress bar
ifneq ($(words $(MAKECMDGOALS)),1)
.DEFAULT_GOAL = all
%:
	@$(MAKE) $@ --no-print-directory -rRf $(firstword $(MAKEFILE_LIST))
else
ifndef ECHO
T := $(shell $(MAKE) $(MAKECMDGOALS) --no-print-directory \
      -nrRf $(firstword $(MAKEFILE_LIST)) \
      ECHO="COUNTTHIS" | grep -c "COUNTTHIS")

N := x
C = $(words $N)$(eval N := x $N)
ECHO = echo "`expr "  [\`expr $C '*' 100 / $T \`" : '.*\(....\)$$'`%]"
endif

CXX=g++

CXXFLAGS:=-w -Iinclude -std=c++11 `sdl2-config --cflags`

SRCS:=$(shell ls | grep .cc)
LIBEMU_SRCS:=$(shell ls base_emu/*.cc)

OBJDIR=objs

OBJS_release:=$(patsubst %.cc, $(OBJDIR)/%_rel.o, $(SRCS))
OBJS_release+=$(patsubst %.cc, $(OBJDIR)/%_rel.o, $(LIBEMU_SRCS))

OBJS_debug:=$(patsubst %.cc, $(OBJDIR)/%_dbg.o, $(SRCS))
OBJS_debug+=$(patsubst %.cc, $(OBJDIR)/%_dbg.o, $(LIBEMU_SRCS))

OBJS_release+=platform_specific.o
OBJS_debug+=platform_specific.o

#LDFLAGS:=-Lbase_emu
LDLIBS:=`sdl2-config --libs`

ifeq ($(OS),Windows_NT)
	SRC_platform:=platform_specific/platform_win32.cc
	CXXFLAGS+=-DOS_WIN
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		SRC_platform:=platform_specific/platform_mac.mm
		CXXFLAGS+=-DOS_MAC
		LDLIBS+=-framework AppKit -framework Cocoa
	else
		SRC_platform:=platform_specific/platform_none.cc
		CXXFLAGS+=-DOS_LINUX
	endif
endif



EXEC_NAME=gba
all: build


platform_specific.o: $(SRC_platform)
	@$(ECHO) Compiling $<
	@mkdir -p $(dir $@)
	@$(CXX) -c $(CXXFLAGS) $(LDLIBS) -o $@ $^

$(OBJDIR)/%_rel.o:%.cc
	@$(ECHO) Compiling $<
	@mkdir -p $(dir $@)
	@$(CXX) -c $(CXXFLAGS) -Ofast $^ -o $@

$(OBJDIR)/%_dbg.o:%.cc
	@$(ECHO) Compiling $<
	@mkdir -p $(dir $@)
	@$(CXX) -c $(CXXFLAGS) -O0 -g $^ -o $@

build: build-rel build-dbg
build-rel: $(EXEC_NAME)_rel
build-dbg: $(EXEC_NAME)_dbg

$(OBJDIR)/%_rel.o:%.cc
	@$(ECHO) Compiling $<
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) -c -Ofast -o $@ $^

$(OBJDIR)/%_dbg.o:%.cc
	@$(ECHO) Compiling $<
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) -c -O0 -g2 -o $@ $^

$(EXEC_NAME)_rel: $(OBJS_release)
	@$(ECHO) Creating release exec
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) -Ofast -o $@ $^ $(LDFLAGS) $(LDLIBS)

$(EXEC_NAME)_dbg: $(OBJS_debug)
	@$(ECHO) Creating debug exec
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) -O0 -g -o $@ $^ $(LDFLAGS) $(LDLIBS)

run: build
	./$(EXEC_NAME)_rel

clean:
	rm -f *.o $(EXEC_NAME)_rel $(EXEC_NAME)_dbg
	rm -rf $(OBJDIR)/*
endif