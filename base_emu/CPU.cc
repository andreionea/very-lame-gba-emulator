#include "CPU.h"
#include "Memory.h"
#include "consts.h"
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

#include "log.h"
#include "interrupts.h"
#include "video.h"
#include "inputhandler.h"
#include "Timer.h"
#include <cstdlib>

using std::pair;

#define WORD       0
#define HALFWORD   1
#define BYTE       2
#define S_HALFWORD 3  
#define S_BYTE     4

#define SET_CPSR_FLAG(flag, value) m_registers[CPSR] = (m_registers[CPSR] & ~(1 << CPU::Flags::flag)) | (((value) & 1) << CPU::Flags::flag)
#define GET_CPSR_FLAG(flag) ((m_registers[CPSR] >> (CPU::Flags::flag)) & 1) 
u32 goodVal;
bool CPU::checkCond(u8 cond)
{
	switch(cond){
		case 0x0: return GET_CPSR_FLAG(Z);  //EQ     Z=1           equal (zero)
		case 0x1: return !GET_CPSR_FLAG(Z);   //NE     Z=0           not equal (nonzero)
		case 0x2: return GET_CPSR_FLAG(C); //CS     C=1           unsigned higher or same (carry set)
		case 0x3: return  !GET_CPSR_FLAG(C);  //CC     C=0           unsigned lower (carry cleared)
		case 0x4: return GET_CPSR_FLAG(N);  //MI     N=1           negative (minus)
		case 0x5: return !GET_CPSR_FLAG(N);  //PL     N=0           positive or zero (plus)
		case 0x6: return GET_CPSR_FLAG(V);  //VS     V=1           overflow (V set)
		case 0x7: return !GET_CPSR_FLAG(V);  //VC     V=0           no overflow (V cleared)
		case 0x8: return !GET_CPSR_FLAG(Z) && GET_CPSR_FLAG(C);  //HI     C=1 and Z=0   unsigned higher
		case 0x9: return GET_CPSR_FLAG(Z) || !GET_CPSR_FLAG(C);  //LS     C=0 or Z=1    unsigned lower or same
		case 0xA: return !(GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V));  //GE     N=V           greater or equal
		case 0xB: return GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V);  //LT     N<>V          less than
		case 0xC: return !GET_CPSR_FLAG(Z) && !(GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V)); //GT     Z=0 and N=V   greater than
		case 0xD: return GET_CPSR_FLAG(Z) | (GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V));  //LE     Z=1 or N<>V   less or equal
		case 0xE: return true;   //AL     -             always
		case 0xF: default: return false;  //NV 
	}
}

CPU::CPU():m_bios(0), m_wram1(0), m_wram2(0), m_ioreg(0), m_vram(0)
		  ,m_gamepack0(0), m_gamepack1(0),m_gamepack2(0),m_sram(0)
		  ,m_isARM(true)
		  ,m_mode(USER)
		  ,m_video(0)
{
	m_video = new Video();
}

void CPU::loadGBA(const char* filename)
{
	m_gamepack0 = new Memory(0x08000000, filename, 0x02000000);
	m_gamepack1 = new Memory(m_gamepack0, 0x0A000000);
	m_gamepack2 = new Memory(m_gamepack0, 0x0C000000);
}

void CPU::Init(const char* bios_filename)
{
	// LOG.enable();
	std::cout << std::setbase(16);
	if (!bios_filename)
		bios_filename = "gba_bios.bin";
	m_bios    = new Memory(0, bios_filename, BIOS_SIZE);
	m_wram1   = new Memory(0x02000000, 0x40000);
	m_wram2   = new Memory(0x03000000, 0x8000);
	m_ioreg   = new Memory(0x04000000, 0x400);
	m_bg_obj  = new Memory(0x05000000, 0x400);
	m_vram    = new Memory(0x06000000, 0x18000);
	m_oam_obj = new Memory(0x07000000, 0x400);
	m_sram    = new Memory(0x0E000000, 0x10000);
	// m_ioreg->SetU8(1, 0x04000300);
	memset(m_registers, 0, sizeof(RegisterSet));
	// m_regs.statusRegs.M = 0x10;
	m_registers[R15] = m_registers[R14] = 0x08000000;
	m_registers[R13_svc] = 0x03007FE0;
    m_registers[R13_irq] = 0x03007FA0;
    m_registers[R13]     = 0x03007F00;

    m_IME = m_ioreg->Fetch16Addr(0x4000208); // Interrupt Master Enable Register
	m_IE  = m_ioreg->Fetch16Addr(0x4000200);  // Interrupt Enable Register
	m_IF  = m_ioreg->Fetch16Addr(0x4000202);  // Interrupt Request Flag Register
	
	m_timers[0] = new Timer(m_ioreg, 0, 0);
	for (int i = 1 ; i < 4; ++i) {
		m_timers[i] = new Timer(m_ioreg, m_timers[i-1], i);
	}	
    m_video->Init(this);
    m_input = new InputHandler(m_ioreg->Fetch16Addr(0x4000130));
    goodVal = m_bios->Fetch32(0x8);
    m_haltCNT = m_ioreg->Fetch8Addr(0x4000301);
    *m_haltCNT = 1;
    *m_IF = 0;
}

Memory* CPU::DeduceMemory(gba_addr addr) const
{
	gba_addr masked = addr & 0xFF000000;

	// LOG << "Masked " << masked << "\n";
	switch(masked) {
		case 0x00000000:
			// LOG << "Bios\n";
			return m_bios;
		case 0x02000000:
			// LOG << "WRAM1\n";
			return m_wram1;
		case 0x03000000:
			// LOG << std::setbase(16) << "WRAM2 " << addr << "\n";
			return m_wram2;
		case 0x04000000:
			// LOG << std::setbase(16) << "IO register " << addr << "\n";
			return m_ioreg; // IO registers, currently not available
		case 0x05000000:
			// LOG << "BG/OBJ\n";
			return m_bg_obj; // BG/OBJ palette
		case 0x06000000:
			// LOG << "VRAM1\n";
			return m_vram;
		case 0x07000000:
			// LOG << "OAM/OBJ\n";
			return m_oam_obj; // OAM/OBJ attributes
		case 0x08000000:
		case 0x09000000:
			// LOG << std::setbase(16) << "Gamepack 1 " << addr << "\n";
			return m_gamepack0;
		case 0x0A000000:
		case 0x0B000000:
			// LOG << "Gamepack 2\n";
			return m_gamepack1;
		case 0x0C000000:
		case 0x0D000000:
			// LOG << "Gamepack 3\n";
			return m_gamepack2;
		case 0x0E000000:
			// LOG << "SRAM\n";
			return m_sram;
//		default:
			// LOG << std::setbase(16) << addr << " nonononononono\n";
	}
	return NULL;
}
void CPU::Step()
{
	LOG << m_registers[R15] << "\n";
	if (m_registers[R15] ==  0x81e0826)
		LOG.enable();
	LOG.flush();
	m_input->UpdateIO();
	// Clear interrupt request flag
	static bool b = true;
	if ((*m_haltCNT)){
		// *m_IF = 0;
		b = true;
		CPU::Instruction instr = NULL;
		if (m_isARM)
			instr = FetchARMInstructionIfNeeded(m_registers[R15]);
		else
			instr = FetchTHUMBInstructionIfNeeded(m_registers[R15]);
		if (instr)
			(this->*instr)();
		else{
			if (m_isARM)
				m_registers[R15] += 4;
			else
				m_registers[R15] += 2;
			LOG.enable();
			LOG << "UNKNOWN INSTRUCTION " << m_registers[R15] << " " << (m_isARM?m_instr : m_instrTHUMB)<< "\n";
			LOG.flush();
			LOG.disable();
		}
		for (int i = R0; i <= R15; ++i)
			LOG << std::setbase(10) << "R" << i << ":" << std::setbase(16) << (int)*getRegister(i) << " ";
		LOG << "\n";
		LOG << "CPSR " << std::setbase(16) << m_regs.rCPSR << "\n";
		if (getRegister(SPSR))
			LOG << "SPSR " << std::setbase(16) << *getRegister(SPSR) << "\n";
		*m_IF = 0;
	}
	m_video->Update();

	CheckTimers();
	CheckDMA();
	CheckInterrupts();
	// LOG << "\n";
}
void CPU::CheckTimers()
{
	for (int i = 0; i < 4; ++i) {
		if (m_timers[i]->Update()) {
		 	*m_IF |= (1 << (IRQ_TIMER0 + i));
		 	std::cout << "Yahtzee " << i << "\n";
		}
	}
}
void CPU::CheckInterrupts()
{
	if (!(*m_IME) || !(*m_IE) || !(*m_IF) || GET_CPSR_FLAG(I) || m_mode == IRQ)
		return;
	u16 IF = *m_IF;
	u16 IE = *m_IE;
	for (u32 i = 0; i <= IRQ_GAMEPACK; ++i)
	{
		if (IF & (1 << i) & IE)
		{
			LOG.enable();
			LOG << "Look at my interrupt " << (int)i << "\n";
			LOG.disable();
			// Change mode to IRQ
			SwitchMode(IRQ);
			*m_haltCNT = 1;
			return;
		}
	}
}

u8 CPU::FetchU8(gba_addr addr)
{
	Memory *mem = DeduceMemory(addr);
	u8 val = mem->Fetch8(addr);
	LOG << "Fetched " << std::setbase(16) << (int)val << "\n";
	return val;
	// return mem->Fetch8(addr);
}

u16 CPU::FetchU16(gba_addr addr)
{
	Memory *mem = DeduceMemory(addr);
	return mem->Fetch16(addr);
}

u32 CPU::FetchU32(gba_addr addr)
{
	Memory *mem = DeduceMemory(addr);
	return mem->Fetch32(addr);
}

DEFINE_COND(B);
void CPU::B(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		u32 offset = (m_instr & 0x00FFFFFF);
		if(offset & 0x00800000) {
		offset |= 0xFF000000;
		}
		offset <<= 2;
		m_registers[R15] += offset + 8;
		LOG << "Offset is " << (int)offset << "\n";
	}
	else
		m_registers[R15] += 4;
}
DEFINE_COND(BL);
void CPU::BL(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		u32 offset = (m_instr & 0x00FFFFFF);
		if(offset & 0x00800000) {
		offset |= 0xFF000000;
		}
		offset <<= 2;
// 		m_registers[R14] = m_registers[R15] + 4;
		*getRegister(R14) = m_registers[R15] + 4;
		m_registers[R15] += offset + 8;
		LOG << "Offset is " << (int)offset << "\n";
	}
	else
		m_registers[R15] += 4;
}
DEFINE_COND(BX);
void CPU::BX(u8 cond)
{
	if (checkCond(cond) )
	{
		if (Rn & 1){
			Rn -= 1;
			m_isARM = false;
			SET_CPSR_FLAG(T, 1);
		}
		LOG << m_instr << "\n";
		LOG << "BX " <<(int) Rn << "\n";

		m_registers[R15] = Rn;
	}
	else 
		m_registers[R15] += 4;

}

DEFINE_COND(STR);
void CPU::STR(u8 cond)
{
	if (checkCond(cond)){
		LOG << std::setbase(16) << "Condition passed " << Rn << " " << Op2 << " " << *Rd << "\n";
		LOG << m_registers[R15] << "\n";
		if (P)
			Rn += (2 * U - 1) * Op2;
		Memory* mem = DeduceMemory(Rn);
		if (mem != m_bios){
			LOG.flush();
			if (BW)
				mem->SetU8(*Rd, Rn);
			else
				mem->SetU32(*Rd, Rn);
			if (!P)
				Rn += (2 * U - 1) * Op2;
			if (W || !P){
				LOG << "writeback\n";
				*pRn = Rn;
			}
			else {
				LOG << "No writeback\n";
				LOG.flush();
			}
		}
	}
	else {
		LOG << "condition failed\n";
	}
	m_registers[R15] += 4;
}

DEFINE_COND(STRHD);
void CPU::STRHD(u8 cond)
{
	if (checkCond(cond)){
		LOG << std::setbase(16) << m_registers[R15] << "\n";
		LOG << std::setbase(16) << "Condition passed " << Rn << " " << *Rd << " " << (int)BW << "\n";
		if (P)
			Rn += (2 * U - 1) * Op2;
		Memory* mem = DeduceMemory(Rn);
		if (mem != m_bios){
			if (BW){
				LOG << "STRHD 16 " << Rn << " " << *Rd << "\n";
				LOG << std::setbase(16) << m_registers[R15] << "\n";
				mem->SetU16(*Rd, Rn);
			}
			else{
				mem->SetU32(*Rd, Rn);
				//TODO: If register is R12, and mode is different from USER, this won't work; however it would be expensive
				mem->SetU32(*(Rd + 1), Rn + 4);
			}
			if (!P)
				Rn += (2 * U - 1) * Op2;
			if (W || !P)
				*pRn = Rn;
		}
	}
	else {
		LOG << "condition failed\n";
	}
	m_registers[R15] += 4;
}
DEFINE_COND(LDR);
void CPU::LDR(u8 cond)
{
	LOG << (int)m_instr << "\n";
	bool condPassed = checkCond(cond);
	if (condPassed){
		LOG << "Condition passed\n";
		LOG << "u " << (int)U << " " << (int) Op2 << " " << (int)Rn << "\n";
		LOG << m_registers[R15] << "\n";
		LOG.flush();
		if (P)
			Rn += (2 * U - 1) * Op2;

		if (BW){
			*Rd = FetchU8(Rn);
			LOG << (int)(Rn) << " rd8 " << (int)*Rd << "\n";
		}
		else {
			*Rd = FetchU32(Rn);
			LOG << (int)(Rn) << " rd32 " << *Rd << "\n";
		}
		if (!P)
			Rn += (2 * U - 1) * Op2;
		if (W || !P){
			LOG << "Writeback\n";
			*pRn = Rn;
		}
	}
	LOG << (int) cond << " " << (int)Rn << " " << (int)(Rd - m_registers) << "\n";
	LOG.flush();
	if (Rd != &m_registers[R15] || !condPassed)
		m_registers[R15] += 4;
}

DEFINE_COND(LDRHD);
void CPU::LDRHD(u8 cond)
{
	LOG << (int)m_instr << "\n";
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		LOG << "u " << (int)U << " " << (int) Op2 << " " << (int)Rn << "\n";
		if (P)
			Rn += (2 * U - 1) * Op2;

		if (BW){
			*Rd = FetchU16(Rn);
			LOG << (int)(Rn) << " rd16 " << (int)*Rd << "\n";
		}
		else {
			*Rd = FetchU32(Rn);
			*(Rd + 1) = FetchU32(Rn + 4);
			Rn += 4;
		}
		if (!P)
			Rn += (2 * U - 1) * Op2;
		if (W || !P)
			*pRn = Rn;
	}
	LOG << (int) cond << " " << (int)Rn << " " << (int)(Rd - m_registers) << "\n";
	if (Rd != &m_registers[R15])	
		m_registers[R15] += 4;
	LOG << "\n\n";
}
DEFINE_COND(SWP);
void CPU::SWP(u8 cond)
{
	if (checkCond(cond)){
		// LOG.enable();
		LOG << "SWP\n";
		// LOG.disable();
		Memory* mem = DeduceMemory(Rn);
		if (BW) {
			u8 val = mem->Fetch8(Rn);
			*Rd = val;
			mem->SetU8(Rs, Rn);
		}
		else {
			u32 val = mem->Fetch32(Rn);
			LOG << (int) Rs << " " << (int)val << "\n";
			*Rd = val;
			mem->SetU32(Rs, Rn);	
		}
	}
	m_registers[R15] += 4;
}
DEFINE_COND(AND);
void CPU::AND(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		rez = Rn & Op2;
		modify = true;
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(XOR);
void CPU::XOR(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		rez = Rn ^ Op2;
		modify = true;
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}
void CPU::checkArithmeticFlags()
{
	u8 sgnA, sgnB, sgnC;

	sgnA = Rn >> 31;
	sgnB = Op2 >> 31;
	sgnC = rez >> 31;
	if (modify)
		*Rd = rez;
	if (m_isARM && (~m_instr & (1 << 20))){
		return;
	}
	SET_CPSR_FLAG(V, (sgnA & sgnB & ~sgnC) | (~sgnA & ~sgnB & sgnC)); // overflow	
	SET_CPSR_FLAG(C, (sgnA & sgnB) | (~sgnC & (sgnA | sgnB))); // carry
	SET_CPSR_FLAG(N, sgnC); // sign
	SET_CPSR_FLAG(Z, rez ? 0: 1);
}
void CPU::checkLogicFlags()
{
	u8 sgn;

	sgn = (rez >> 31) & 1;
	if (modify)
		*Rd = rez;
	if (m_isARM && (~m_instr & (1 << 20))){
		return;
	}
	SET_CPSR_FLAG(N, sgn); // sign
	SET_CPSR_FLAG(Z, rez ? 0: 1);	
}
DEFINE_COND(SUB);
void CPU::SUB(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		modify = true;
		Op2 = ~Op2 + 1;
		rez = Rn + Op2;
		checkArithmeticFlags();
	}
	if (Rd != &m_registers[R15])
		m_registers[R15] += 4;
	else {
		DataProcessUseR15();
	}
}

DEFINE_COND(RSB);
void CPU::RSB(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		LOG << Rn << " " << Op2 << "\n";
		modify = true;
		Rn = ~Rn + 1;
		rez = Rn + Op2;
		LOG << rez << " " << m_registers[R15] <<  "\n";
		checkArithmeticFlags();
		LOG << m_registers[CPSR] << "\n";
	}
	m_registers[R15] += 4;
}

DEFINE_COND(ADD);
void CPU::ADD(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		modify = true;	
		rez = Rn + Op2;
		LOG << Rn << " + " << Op2 << " Result is " << rez << "\n";
		LOG << "Result put in R" << Rd - m_registers << "\n";
		checkArithmeticFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(ADC);
void CPU::ADC(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		modify = true;	
		rez = Rn + Op2 + GET_CPSR_FLAG(C);
		LOG << Rn << " + " << Op2 << " Result is " << rez << "\n";
		LOG << "Result put in R" << Rd - m_registers << "\n";
		checkArithmeticFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(SBC);
void CPU::SBC(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		modify = true;
		Op2 = ~Op2 + 1;
		rez = Rn + Op2 + GET_CPSR_FLAG(C) - 1;
		checkArithmeticFlags();	
	}
	m_registers[R15] += 4;
}

DEFINE_COND(RSC);
void CPU::RSC(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		modify = true;
		Rn = ~Rn + 1;
		rez = Rn + Op2 + GET_CPSR_FLAG(C) - 1;
		checkArithmeticFlags();	
	}
	m_registers[R15] += 4;
}

DEFINE_COND(TST);
void CPU::TST(u8 cond)
{
	if (checkCond(cond)){
		modify = false;
		LOG << "Condition passed\n";
		rez = Rn & Op2;
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(TEQ);
void CPU::TEQ(u8 cond)
{
	if (checkCond(cond)){	
		modify = false;
		LOG << "Condition passed\n";
		rez = Rn ^ Op2;
		LOG << (int)Rn << " " << (int)Op2 << "\n";
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(CMP);
void CPU::CMP(u8 cond)
{
	LOG << "CMP\n";
	if (checkCond(cond)){	
		modify = false;
		LOG << "Condition passed\n";
		Op2 = ~Op2 + 1;
		rez = Rn + Op2;
		checkArithmeticFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(CMN);
void CPU::CMN(u8 cond)
{
	if (checkCond(cond)){
		modify = false;
		LOG << "Condition passed\n";
		rez = Rn + Op2;
		checkArithmeticFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(OR);
void CPU::OR(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		*Rd = Rn | Op2;
		rez = *Rd;
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}

void CPU::DataProcessUseR15()
{
	m_registers[CPSR] = *getRegister(SPSR);
	if (GET_CPSR_FLAG(T))
		m_isARM = false;
	else
		m_isARM = true;
	switch(m_regs.rCPSR & 0x1F) {
			case 0x10:
				m_mode = USER;
				LOG << "Changed to USER mode\n";
				break;
			case 0x11:
				m_mode = FIQ;
				LOG << "Changed to FIQ mode\n";
				break;
			case 0x12:
				m_mode = IRQ;
				LOG << "Changed to irq mode\n";
				break;
			case 0x13:
				m_mode = SUPERVISOR;
				LOG << "Changed to SUPERVISOR mode\n";
				break;
			default:
				m_mode = USER;
				LOG << "Changed to USER mode\n";						
		}
}

DEFINE_COND(MOV);
void CPU::MOV(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		LOG << (int) (Rd - m_registers) << "  " << (int)Op2 << " rn\n";
		*Rd = Op2;
		rez = *Rd;
		modify = false;
		checkLogicFlags();
		if ((m_instr & (1 << 20)) && Rd == getRegister(R15)) {
			LOG << "OMGQTFBBQ\n";
			DataProcessUseR15();
			return;
		}
	}
	if (Rd != &m_registers[R15])
	m_registers[R15] += 4;
}

DEFINE_COND(BIC);
void CPU::BIC(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		// Rd = Rn AND NOT Op2
		*Rd = Rn & ~Op2;
		rez = *Rd;
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(MVN);
void CPU::MVN(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		*Rd = ~Op2;
		rez = *Rd;
		modify = false;
		checkLogicFlags();
	}
	m_registers[R15] += 4;
}

DEFINE_COND(MSR);
void CPU::MSR(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		if (!Psr) {
			if (flags){
				m_regs.rCPSR &= ~(0xFF000000);
				m_regs.rCPSR |= *getRegister(Rn) & 0xFF000000;
			}
			if (control){// && m_mode != USER){
				m_regs.rCPSR &= ~(0xFF);
				m_regs.rCPSR |= *getRegister(Rn) & 0xFF;
				switch(m_regs.rCPSR & 0x1F) {
					case 0x10:
						m_mode = USER;
						LOG << "Changed to USER mode\n";
						break;
					case 0x11:
						m_mode = FIQ;
						LOG << "Changed to FIQ mode\n";
						break;
					case 0x12:
						m_mode = IRQ;
						LOG << "Changed to irq mode\n";
						break;
					case 0x13:
						m_mode = SUPERVISOR;
						LOG << "Changed to SUPERVISOR mode\n";
						break;
					default:
						m_mode = USER;
						LOG << "Changed to USER mode\n";						
				}
			}
		}
		else {
			if (flags){
				u32 *spsr = getRegister(SPSR);
				if (spsr){
					*spsr &= ~(0xFF000000);
					*spsr |= *getRegister(Rn) & 0xFF000000;
				}
//				m_regs.SPSR &= ~(0xFF000000);
// 				m_regs.rSPSR |= m_registers[Rn] & 0xFF000000;
			}
			if (control){
				u32 *spsr = getRegister(SPSR);
				if (spsr){
					*spsr &= ~(0xFF);
					*spsr |= *getRegister(Rn) & 0xFF;
				}
//				m_regs.rSPSR &= ~(0xFF);
// 				m_regs.rSPSR |= m_registers[Rn] & 0xFF;
			}
		}
		LOG << std::setbase(16) << (int)m_regs.rCPSR << "\n";
	}
	m_registers[R15] += 4;
}

DEFINE_COND(MRS);
void CPU::MRS(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		if (!Psr)
			*Rd = m_regs.rCPSR;
		else
			*Rd = *getRegister(SPSR);
	}
	m_registers[R15] += 4;
}

DEFINE_COND(SWI);
void CPU::SWI(u8 cond)
{
	if (checkCond(cond)){
		SwitchMode(SUPERVISOR);
	}
	else 
		m_registers[R15] += 4;
}

DEFINE_COND(STM);
void CPU::STM(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		u32 base = *pRn;
		LOG << "base is " << pRn - &m_registers[R0] << " and rlist is " << Rlist << "\n";
		LOG << "write\n";
		Memory* mem = DeduceMemory(base);
		int inc = (2 * U - 1);
		int firstReg, lastReg;
		if (inc == -1) {
			firstReg = R15;
			lastReg = R0 - 1;
		}
		else
		{
			firstReg = R0;
			lastReg = R15 + 1;
		}
		for (int i = firstReg; i != lastReg; i += inc)
			if (Rlist & (1 << i)){
				LOG << "STM " << i << "\n";
				if (P)
					base += inc * 4;
				mem->SetU32(*getRegister(i), base);
				if (!P)
					base += inc * 4;
			}
		if (W){
			LOG << "writeback\n";
			*pRn = base;
		}
		else
			LOG << "no writeback\n";
	}
	m_registers[R15] += 4;
}

DEFINE_COND(LDM);
void CPU::LDM(u8 cond)
{
	if (checkCond(cond)){
		LOG << "Condition passed\n";
		u32 base = *pRn;
		Memory* mem = DeduceMemory(base);
		int inc = (2 * U - 1);
		int firstReg, lastReg;
		if (inc == -1) {
			firstReg = R15;
			lastReg = R0 - 1;
		}
		else
		{
			firstReg = R0;
			lastReg = R15 + 1;
		}
		for (int i = firstReg; i != lastReg; i += inc)
			if (Rlist & (1 << i)) {
				if (P)
					base += inc * 4;
				LOG << "LDM " << i << "\n";
				*getRegister(i) = mem->Fetch32(base);
				if (!P)
					base += inc * 4;
			}
		if (W) {
			*pRn = base;
		}
	}
	if (!(Rlist & (1 << R15)))
		m_registers[R15] += 4;
}

DEFINE_COND(MUL);
void CPU::MUL(u8 cond) 
{
	if (checkCond(cond))
	{
		// TODO: Also consider the case where S is set
		*Rd = Rn * Rs;
	}
	m_registers[R15] += 4;
}

DEFINE_COND(MLA);
void CPU::MLA(u8 cond) 
{
	if (checkCond(cond))
	{
		// TODO: Also consider the case where S is set
		*Rd = Rn * Rs + Op2;
	}
	m_registers[R15] += 4;
}
DEFINE_COND(UMULL);
void CPU::UMULL(u8 cond)
{
	if (checkCond(cond)) {
		unsigned long long rez = Rs * Rn;
		*Rd = (rez >> 32);
		*pOp2 = (rez & 0xFFFFFFFF);
	}
	m_registers[R15] += 4;
}

DEFINE_COND(UMLAL);
void CPU::UMLAL(u8 cond)
{
	if (checkCond(cond)) {
		unsigned long long rez;
		rez = *Rd;
		rez = (rez << 32) | *pOp2;
		rez += Rs * Rn;

		*Rd = (rez >> 32);
		*pOp2 = (rez & 0xFFFFFFFF);
	}
	m_registers[R15] += 4;
}

DEFINE_COND(SMULL);
void CPU::SMULL(u8 cond)
{
	if (checkCond(cond)) {
		u8 sgn = ((Rs >> 31) & 1) ^ ((Rn >> 31) & 1);
		if (Rs & (1 << 31))
			Rs = ~Rs + 1;
		if (Rn & (1 << 31))
			Rn = ~Rn + 1;
		unsigned long long rez = Rs * Rn;
		if (sgn)
			rez = ~rez + 1;
		*Rd = (rez >> 32);
		*pOp2 = (rez & 0xFFFFFFFF);
	}
	m_registers[R15] += 4;
}
void CPU::T_LSL()
{
	LOG << "T_LSL\n";
	Rn = Rs;
	rez = Rs << Op2;
	modify = true;
	checkArithmeticFlags();
	if (rez < Rs)
		SET_CPSR_FLAG(C, 1);
	m_registers[R15] += 2;
}
void CPU::T_LSR()
{
	LOG << "T_LSR\n";
	Rn = Rs;
	LOG << Op2 << "\n" ;
	rez = Rs >> Op2;
	modify = true;
	u8 flag = GET_CPSR_FLAG(C);
	checkArithmeticFlags();
	if (!Op2)
		SET_CPSR_FLAG(C, flag);
	else if (Op2 >= 33)
		SET_CPSR_FLAG(C, 0);
	else
		SET_CPSR_FLAG(C, (Rn >> (Op2 - 1)) & 1);
	m_registers[R15] += 2;
}
void CPU::T_ASR()
{
	LOG << "T_ASR\n";
	rez = Rs >> Op2;
	if (Rs & (1 << 31)) {
		rez |= 0 - (0x8ffffff >> Op2);
	}
	modify = true;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_ADD()
{
	LOG << "T_ADD\n";
	rez = Rn + Op2;
	modify = true;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_SUB()
{
	LOG << "T_SUB " << Rn << " " << Op2 << "\n";
	Op2 = ~Op2 + 1;
	rez = Rn + Op2;
	modify = true;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_MOV()
{
	LOG << "T_MOV\n";
	*Rd = Op2;
	rez = Op2;
	modify = false;
	checkLogicFlags();
	if (Rd == &m_registers[R15]){
		*Rd &= -2;
	}
	else
		m_registers[R15] += 2;
}
void CPU::T_CMP()
{
	LOG << "T_CMP\n";
	Rn = *Rd;
	rez = Rn - Op2;
	Op2 = ~Op2 + 1;
	LOG << (int)(Rn) << " " << (int)Op2 << " " << rez << "\n";
	modify = false;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_AND()
{
	LOG << "T_AND\n";
	rez = *Rd & Op2;
	modify = true;
	checkLogicFlags();
	m_registers[R15] += 2;
}
void CPU::T_EOR()
{
	LOG << "T_EOR\n";
	rez = *Rd ^ Op2;
	modify = true;
	checkLogicFlags();
	m_registers[R15] += 2;
}
void CPU::T_ADC()
{
	// LOG.enable();
	LOG << "T_ADC\n";
	LOG << "Unimplemented op T_ADC\n";
	// LOG.disable();
	m_registers[R15] += 2;
}
void CPU::T_SBC()
{
	// LOG.enable();
	LOG << "T_SBC\n";
	LOG << "Unimplemented op T_SBC\n";
	// LOG.disable();
	m_registers[R15] += 2;
}
void CPU::T_ROR()
{
	LOG << "T_ROR\n";
	Op2 &= 0xFF;
	rez = (*Rd >> Op2) | (*Rd << (32 - Op2));
	modify = true;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_TST()
{
	LOG << "T_TST\n";
	rez = *Rd & Op2;
	modify = false;
	checkLogicFlags();
	m_registers[R15] += 2;
}
void CPU::T_NEG()
{
	LOG << "T_NEG\n";
	*Rd = 0 - *Rd;
	rez = *Rd;
	modify = false;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_CMN()
{
	// LOG.enable();
	LOG << "T_CMN\n";
	LOG << "Unimplemented op T_CMN\n";
	// LOG.disable();
	m_registers[R15] += 2;
}
void CPU::T_ORR()
{
	LOG << "T_ORR\n";
	Rn = *Rd;
	*Rd |= Op2;
	rez = *Rd;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_MUL()
{
	LOG << "T_MUL\n";
	*Rd = (*Rd) * Op2;
	rez = *Rd;
	modify = false;
	checkArithmeticFlags();
	m_registers[R15] += 2;
}
void CPU::T_BIC()
{
	LOG << "T_BIC\n";
	*Rd = Rn & (~Op2);
	rez = *Rd;
	modify = false;
	checkLogicFlags();
	m_registers[R15] += 2;
}
void CPU::T_MVN()
{
	LOG << "T_MVN\n";
	*Rd = ~Op2;
	rez = *Rd;
	modify = false;
	checkLogicFlags();
	m_registers[R15] += 2;
}
void CPU::T_LDR()
{
	LOG << "T_LDR\n";
	switch(W){
		case WORD:
			LOG << "WORD " << (int) (Rd - m_registers);
			*Rd = FetchU32(Op2);
			break;
		case HALFWORD:
			LOG << "HALFWORD\n";
			*Rd = FetchU16(Op2);
			break;
		case BYTE:
			LOG << "BYTE\n";
			*Rd = FetchU8(Op2);
			break;
		case S_HALFWORD:
			LOG << "S_HALFWORD\n";
			*Rd = FetchU16(Op2);
			*Rd = ((-((*Rd >>15) & 1)) & 0xFF00) | *Rd;
			break;
		case S_BYTE:
			LOG << "S_BYTE\n";
			*Rd = FetchU8(Op2);
			*Rd = ((-((*Rd >>7) & 1)) & 0xFFF0) | *Rd;
			break;
		
	}
	m_registers[R15] += 2;
}
void CPU::T_STR()
{
	Memory* mem = DeduceMemory(Op2);
	LOG << "T_STR\n";	
	std::cout.flush();
	if (mem != m_bios)
		switch(W){
			case WORD:
				mem->SetU32(*Rd, Op2);
				break;

			case HALFWORD:
				mem->SetU16(*Rd, Op2);
				break;
			case BYTE:
				mem->SetU8(*Rd, Op2);
				break;
			case S_HALFWORD:
			{
				u16 val = ((-((*Rd >>15) & 1)) & 0xFF00) | *Rd;
				mem->SetU16(val, Op2);
				break;
			}
			case S_BYTE:
				u16 val2 = ((-((*Rd >>7) & 1)) & 0xFFF0) | *Rd;
				mem->SetU8(val2, Op2);		
		}
	m_registers[R15] += 2;
}


void CPU::T_BX()
{
	LOG << "T_BX\n";
	if (!(Op2 & 1)){	
		m_isARM = true;
		m_registers[R15] = Op2 & (~0x2);
		SET_CPSR_FLAG(T, 0);
	}
	else{
		m_registers[R15] = Op2 - 1;
	}
}

void CPU::T_BL()
{
	LOG << "T_BL\n";
	LOG << Rn << "\n";
	// u16 nn = (Rn >> 16) & 0x3FF;
	
	u32 nn = Rn & 0x7FF;
	nn |= 0 - (nn & 0x400);
	*getRegister(R14) = m_registers[R15] + 4 + (nn << 12);
	u32 temp = m_registers[R15] + 4;
	u8 opcode = (Rn >> 11) & 0x1F;
	if (opcode == 0x1D) {
		m_isARM = true;
	}
	nn = (Rn >> 16) & 0x7FF;
// 	m_registers[R15] = m_registers[R14] + (nn << 1);
// 	m_registers[R14] = temp | 1;
	m_registers[R15] = *getRegister(R14) + (nn << 1);
	*getRegister(R14) = temp | 1;

}
void CPU::T_B()
{
	LOG << "T_B\n";
	LOG << (int)Rn << "\n";
	m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x200))) | (Rn << 1));
}
void CPU::T_BEQ()
{
	LOG << "T_BEQ\n";
	bool condition = GET_CPSR_FLAG(Z);
	if (condition){
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
		LOG << "Condition passed\n";
	}
	else
		m_registers[R15] += 2;

}
void CPU::T_BNE()
{
	LOG << "T_BNE\n";
	bool condition = !GET_CPSR_FLAG(Z);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BCS()
{
	LOG << "T_BCS\n";
	bool condition = GET_CPSR_FLAG(C);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BCC()
{
	LOG << "T_BCC\n";
	bool condition = !GET_CPSR_FLAG(C);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BMI()
{
	LOG << "T_BMI\n";
	bool condition = GET_CPSR_FLAG(N);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BPL()
{
	LOG << "T_BPL\n";
	bool condition = !GET_CPSR_FLAG(N);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BVS()
{
	LOG << "T_BVS\n";
	bool condition = GET_CPSR_FLAG(V);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BVC()
{
	LOG << "T_BVC\n";
	bool condition = !GET_CPSR_FLAG(V);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BHI()
{
	LOG << "T_BHI\n";
	bool condition = !GET_CPSR_FLAG(Z) && GET_CPSR_FLAG(C);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BLS()
{
	LOG << "T_BLS\n";
	bool condition = GET_CPSR_FLAG(Z) || !GET_CPSR_FLAG(C);
	if (condition){
		LOG << "Condition passed\n";
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	}
	else
		m_registers[R15] += 2;

}
void CPU::T_BGE()
{
	LOG << "T_BGE\n";
	bool condition = !(GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V));
	if (condition){
		LOG << "Condition passed\n";
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	}
	else
		m_registers[R15] += 2;

}
void CPU::T_BLT()
{
	LOG << "T_BLT\n";
	bool condition = GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V);
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;
}
void CPU::T_BGT()
{
	LOG << "T_BGT\n";
	bool condition = !GET_CPSR_FLAG(Z) && !(GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V));
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}
void CPU::T_BLE()
{
	LOG << "T_BLE\n";
	bool condition = GET_CPSR_FLAG(Z) | (GET_CPSR_FLAG(N) ^ GET_CPSR_FLAG(V));
	if (condition)
		m_registers[R15] += 4 + (((u32)(0 - (Rn & 0x80))) | (Rn << 1));
	else
		m_registers[R15] += 2;

}

void CPU::SwitchMode(CPUMode mode)
{
	m_mode = mode;
	*getRegister(SPSR) = m_registers[CPSR];
	m_registers[CPSR] &= ~(0x1F);
	u8 mask = 0;
	u32 exceptionVector;
	switch(mode) {
		case USER:
			exceptionVector = m_registers[R15];
			mask = 0x10;
			break;
		case FIQ:
			exceptionVector = 0x1c;
			mask = 0x10;
			break;
		case IRQ:
			exceptionVector = 0x18;
			mask = 0x12;
			break;
		case SUPERVISOR:
			exceptionVector = 0x8;
			mask = 0x13;
			break;
	}

	m_registers[CPSR] |= mask;
	*getRegister(R14) =  m_registers[R15];
	if (mode == SUPERVISOR){	
		if (m_isARM)
			*getRegister(R14) += 4;
		else
			*getRegister(R14) += 2;

		if (!m_isARM)
			SET_CPSR_FLAG(I, 1);
	}
	else if (mode == IRQ) {
		*getRegister(R14) += 4;
	}
	
	m_isARM = true;

	m_registers[R15] = exceptionVector;
}

void CPU::T_SWI()
{
	LOG.enable();
	LOG << std::setbase(16) <<  "T_SWI " << (m_instrTHUMB & 0xff) << " " << m_registers[R15] << " " << m_registers[R0] <<" " << m_registers[R1] << "\n";
	SwitchMode(SUPERVISOR);
}

void CPU::T_PUSH()
{
	LOG << "T_PUSH\n";
// 	u32 base = m_registers[R13];
	u32 base = *getRegister(R13);
	LOG << base << "\n";
	Memory* mem = DeduceMemory(base);
	if (P) {
		base -= 4;
		mem->SetU32(*getRegister(R14), base);
	}
	for (int i = 7; i >= 0; --i){
		if (Rlist & (1 << i)){
			base -= 4;
			mem->SetU32(*getRegister(R0 + i), base);
		}
	}
	
// 	m_registers[R13] = base;
	*getRegister(R13) = base;
	m_registers[R15] += 2;
}

void CPU::T_POP()
{
	LOG << "T_POP\n";
	u32 base = *getRegister(R13);
	Memory* mem = DeduceMemory(base);
	for (int i = 0; i <= 7; ++i){
		if (Rlist & (1 << i)){
			*getRegister(R0 + i) = mem->Fetch32(base);
			base += 4;
		}
	}
	if (P) {
		*getRegister(R15) = mem->Fetch32(base) & ~1;
		base += 4;
	}
// 	m_registers[R13] = base;
	*getRegister(R13) = base;
	if (!P)
		m_registers[R15] += 2;
}
void CPU::T_LDMIA()
{
	LOG << "T_LDMIA\n";
	Memory* mem = DeduceMemory(*Rd);	
	for (int i = R0; i <= R7; ++i) {
		if (Rlist & (1 << i)){
			*getRegister(i) = mem->Fetch32(*Rd);
			LOG << i << " " << *getRegister(i) << "\n";
			*Rd += 4;
		}
	}
	m_registers[R15] += 2;
}
void CPU::T_STMIA()
{
	LOG << "T_STMIA\n";
	Memory* mem = DeduceMemory(*Rd);
	for (int i = R0; i <= R7; ++i) {
		if (Rlist & (1 << i)){
			mem->SetU32((u32)(*getRegister(i)), *Rd);
			*Rd += 4;
		}
	}
	m_registers[R15] += 2;
}

CPU::Instruction CPU::FetchTHUMBInstructionIfNeeded(gba_addr addr)
{
	m_instrTHUMB = FetchU16(addr);
	CPU::Instruction instr = NULL;
	// TODO: add caching
	LOG << "Fetching THUMB instr " << std::setbase(16) << (int) m_instrTHUMB << " " << addr << "\n";
	u8 prefix = (m_instrTHUMB >> 13) & 0x7;
	switch(prefix) {
	case 0x0:
	{
		u8 opcode = (m_instrTHUMB >> 11) & 0x3;
		Op2 = (m_instrTHUMB >> 6) & 0x1F;
		Rs = *getRegister((m_instrTHUMB >> 3) & 0x7);
// 		Rd = &m_registers[m_instrTHUMB & 0x7];
		Rd = getRegister(m_instrTHUMB & 0x7);
		// TODO: For LSR, this might not make sense
		if (Op2)
			SET_CPSR_FLAG(C, 1);
		switch(opcode){
		case 0x0:
			instr = &CPU::T_LSL;
			break;
		case 0x1:
			instr = &CPU::T_LSR;
			break;
		case 0x2:
			instr = &CPU::T_ASR;
			break;
		case 0x3:{
			u8 opcode2 = (m_instrTHUMB >> 9) & 0x3;
// 			Rd = &m_registers[m_instrTHUMB & 0x7];
// 			Rn = m_registers[(m_instrTHUMB >> 3) & 0x7];
			Rd = getRegister(m_instrTHUMB & 0x7);
			Rn = *getRegister((m_instrTHUMB >> 3) & 0x7);
			switch(opcode2){
			case 0x0:
// 				Op2 = m_registers[(m_instrTHUMB >> 6) & 0x7];
				Op2 = *getRegister((m_instrTHUMB >> 6) & 0x7);
				instr = &CPU::T_ADD;
				break;
			case 0x1:
// 				Op2 = m_registers[(m_instrTHUMB >> 6) & 0x7];
				Op2 = *getRegister((m_instrTHUMB >> 6) & 0x7);
				instr = &CPU::T_SUB;
				break;
			case 0x2:
				Op2 = (m_instrTHUMB >> 6) & 0x7;
				instr = &CPU::T_ADD;
				break;
			case 0x3:
				Op2 = (m_instrTHUMB >> 6) & 0x7;
				instr = &CPU::T_SUB;
				break;
			}
			break;		
			}
		}
		break;
	}
	case 0x1:{
		Op2 = (m_instrTHUMB) & 0xFF;
// 		Rd = &m_registers[R0 + (m_instrTHUMB >> 8) & 0x7];
		Rd = getRegister(R0 + (m_instrTHUMB >> 8) & 0x7);
		Rn = *Rd;
		u8 opcode = (m_instrTHUMB >> 11) & 0x3;
		Instruction instrs[] = {&CPU::T_MOV, &CPU::T_CMP, &CPU::T_ADD, &CPU::T_SUB};
		instr = instrs[opcode];
		break;
	}
	case 0x2:{
		LOG << "2\n";
		bool isALU = ((m_instrTHUMB >> 10) & 0x3F) == 0x10;
		bool isHighReg = ((m_instrTHUMB >> 10) & 0x3F) == 0x11;
		bool isLoadPCRelative = ((m_instrTHUMB >> 11) & 0x1f) == 0x9;
		bool isWithRegisterOffset = ((m_instrTHUMB >> 12) &  0xF)== 0x5;
		Instruction instrs_ALU [] = {&CPU::T_AND, &CPU::T_EOR, &CPU::T_LSL, &CPU::T_LSR, 
									 &CPU::T_ASR, &CPU::T_ADC, &CPU::T_SBC, &CPU::T_ROR, 
									 &CPU::T_TST, &CPU::T_NEG, &CPU::T_CMP, &CPU::T_CMN, 
									 &CPU::T_ORR, &CPU::T_MUL, &CPU::T_BIC, &CPU::T_MVN};
		Instruction instrs_HighReg[] = {&CPU::T_ADD, &CPU::T_CMP, &CPU::T_MOV, &CPU::T_BX};

		if (isALU){
// 			Rd = &m_registers[m_instrTHUMB & 0x7];
// 			Op2 = m_registers[(m_instrTHUMB >> 3) & 0x7];
			Rd = getRegister(m_instrTHUMB & 0x7);
			Op2 = *getRegister((m_instrTHUMB >> 3) & 0x7);
			u8 opcode = (m_instrTHUMB >> 6) & 0xF;
			instr = instrs_ALU[opcode];
		}
		else if (isHighReg){
			u8 MSBD = (m_instrTHUMB  >> 7) & 1;
			u8 MSBS = (m_instrTHUMB  >> 6) & 1;
			u8 opcode = (m_instrTHUMB >> 8) & 0x3;
// 			Rd = &m_registers[(MSBD << 3) | (m_instrTHUMB & 0x7)];
// 			Op2 = m_registers[(MSBS << 3) | ((m_instrTHUMB >> 3) & 0x7)];
			Rd = getRegister((MSBD << 3) | (m_instrTHUMB & 0x7));
			Rn = *Rd;
			Op2 = *getRegister((MSBS << 3) | ((m_instrTHUMB >> 3) & 0x7));
			if (((MSBS << 3) | ((m_instrTHUMB >> 3) & 0x7)) == 15)
				Op2 += 4; 
			instr = instrs_HighReg[opcode];
		}
		else if (isLoadPCRelative) {
// 			Rd = &m_registers[(m_instrTHUMB >> 8) & 0x7];
			Rd = getRegister((m_instrTHUMB >> 8) & 0x7);
			Op2 = ((m_instrTHUMB & 0xFF) << 2) + ((m_registers[R15] + 4) & ~2);
			LOG << ((m_instrTHUMB & 0xFF) << 2) << "\n";
			W = WORD;
			instr = &CPU::T_LDR;
		}
		else if (isWithRegisterOffset) {
			LOG << "Register with offset\n";
// 			Op2 = m_registers[(m_instrTHUMB >> 3) & 0x7] + m_registers[(m_instrTHUMB >> 6) & 0x7];
			Op2 = *getRegister((m_instrTHUMB >> 3) & 0x7) + m_registers[(m_instrTHUMB >> 6) & 0x7];
			u8 opcode = (m_instrTHUMB >> 10) & 0x3;
			P = m_instrTHUMB & 0x200;
// 			Rd = &m_registers[m_instrTHUMB & 0x7];
			Rd = getRegister(m_instrTHUMB & 0x7);
			switch(opcode) {
				case 0x0:
					W = WORD;
					if (P)
						W = HALFWORD;
					instr = &CPU::T_STR;
					break;
				case 0x1:
					if (P){
						W = S_BYTE;
						instr = &CPU::T_LDR;
					}
					else {	
						W = BYTE;
						instr = &CPU::T_STR;
					}
					break;
				case 0x2:
					W = WORD;
					if (P)
						W = HALFWORD;
					instr = &CPU::T_LDR;
					break;
				case 0x3:
					W = BYTE;
					if (P)
						W = S_HALFWORD;
					instr = &CPU::T_LDR;
			}
		}
		break;
	}
	case 0x3:{
		u32 opcode = (m_instrTHUMB >> 11) & 0x3;
		Rd = getRegister(R0 + (m_instrTHUMB & 0x7));
		Op2 = *getRegister(R0 + ((m_instrTHUMB >> 3) & 0x7)) + ((m_instrTHUMB >> 6) & 0x1F);
		if (opcode < 2){
			W = WORD;
			Op2 = *getRegister(R0 + ((m_instrTHUMB >> 3) & 0x7)) + 4 * ((m_instrTHUMB >> 6) & 0x1F);
		}
		else
			W = BYTE;
		if ((opcode % 2) == 0)
			instr = &CPU::T_STR;
		else
			instr = &CPU::T_LDR;
		break;
	}
	case 0x4:
		if ((m_instrTHUMB >> 12) & 1) {
			W = WORD;
// 			Op2 = m_registers[R13] + (m_instrTHUMB & 0xfF) * 4;
// 			Rd = &m_registers[R0 + ((m_instrTHUMB >> 8) & 0xf)];
			Op2 = *getRegister(R13) + ((m_instrTHUMB & 0xFF) << 2);
			Rd = getRegister(R0 + ((m_instrTHUMB >> 8) & 0x7));
		}
		else {
			W = HALFWORD;
			Rd = getRegister(m_instrTHUMB & 0x7);
			Op2 = *getRegister(R0 + ((m_instrTHUMB >> 3) & 0x7)) + (((m_instrTHUMB >> 6) & 0x1F) * 2);
		}
		if (m_instrTHUMB & (1<< 11))
				instr = &CPU::T_LDR;
			else
				instr = &CPU::T_STR;

		break;
	case 0x5:
		if ((m_instrTHUMB >> 12) & 1) {
			if (((m_instrTHUMB >> 9) & 0x3) == 0x2){
				Rlist = m_instrTHUMB & 0xFF;
				P = (m_instrTHUMB >> 8) & 1;
				if ((m_instrTHUMB >> 11) & 1)
					instr = &CPU::T_POP;
				else
					instr = &CPU::T_PUSH;
			}
			else if (((m_instrTHUMB  >> 8) & 0xFF) == 0xb0)
			{
				Rd = getRegister(R13);
				Rn = *Rd;
				Op2 = (m_instrTHUMB & 0x3F) << 2;
				if (m_instrTHUMB & (1 << 7))
					instr = &CPU::T_SUB;
				else
					instr = &CPU::T_ADD;
					
			}
		}
		else {
			Rd = getRegister((m_instrTHUMB >> 8) & 0x7);
			Op2 = (m_instrTHUMB & 0xFF) << 2;
			if ((m_instrTHUMB >> 11) & 1) {
				Rn = *getRegister(R13);
			}
			else {
				Rn = (m_registers[R15] + 4) & (~2);
			}
			instr = &CPU::T_ADD;
		}
		break;
	case 0x6:
		if ((m_instrTHUMB >> 12) & 1){
			Rn = m_instrTHUMB & 0xff;
			Instruction instrs[] = {
				&CPU::T_BEQ,
				&CPU::T_BNE,
				&CPU::T_BCS,
				&CPU::T_BCC,
				&CPU::T_BMI,
				&CPU::T_BPL,
				&CPU::T_BVS,
				&CPU::T_BVC,
				&CPU::T_BHI,
				&CPU::T_BLS,
				&CPU::T_BGE,
				&CPU::T_BLT,
				&CPU::T_BGT,
				&CPU::T_BLE,
				&CPU::T_SWI,
				&CPU::T_SWI};
			instr = instrs[(m_instrTHUMB >> 8) & 0xF];
		}
		else {
			Rd = getRegister((m_instrTHUMB >> 8) & 0x7);
			Rlist = m_instrTHUMB & 0xFF;
			if ((m_instrTHUMB >> 11) & 1)
				instr = &CPU::T_LDMIA;
			else
				instr = &CPU::T_STMIA;
		}
		break;
	case 0x7:
		{
			u8 op = (m_instrTHUMB >> 11) & 3;
			switch (op) {
				case 0:
					Rn = m_instrTHUMB & 0x3FF;
					instr = &CPU::T_B;
					break;
				case 1:
					break;
				case 2:
					Rn = FetchU32(m_registers[R15]);
					instr = &CPU::T_BL;
					break;
				case 3:
					break;
			}
		}
		break;
	}
	// m_registers[R15] += 2;
	return instr;
}

u32* CPU::getRegister(unsigned int reg)
{
	switch(m_mode) {
		case USER:
			return m_registers + reg;
		case FIQ:
			if (reg < R8 || reg == R15)
				return m_registers + reg;
			return m_registers + reg - R8 + R8_fiq;
		case SUPERVISOR:
			if (reg < R13 || reg == R15)
				return m_registers + reg;
			return m_registers + reg - R13 + R13_svc;
		case IRQ:
			if (reg < R13 || reg == R15)
				return m_registers + reg;
			return m_registers + reg - R13 + R13_irq;
	}
	return 0;
}

CPU::Instruction CPU::FetchARMInstructionIfNeeded(gba_addr addr)
{
	modify = false;
	map<gba_addr, CPU::Instruction>::iterator it = m_jit.find(addr);
	if (it == m_jit.end()) {
		m_instr = FetchU32(addr);
		LOG << "Fetched arm instr " << std::setbase(16) << m_instr  << " at address "  << addr << "\n";
		LOG << "instr " << std::setbase(16) << m_instr <<  " at " << addr << "\n";
		if (GET_CPSR_FLAG(C)) {
			LOG << "Carry flag\n";
		}
		else {
			LOG << "No Carry flag\n";
		}
		u8 condition = (m_instr >> 28) & 0xF;
		CPU::Instruction instr = NULL;

		u8 type = (m_instr >> 24) & 0xF;
// 		Rn = m_registers[(m_instr >> 16) & 0xF];
// 		pRn = &m_registers[(m_instr >> 16) & 0xF];
// 		Rd = &m_registers[R0 + (m_instr >> 12) & 0xF];
		pRn = getRegister((m_instr >> 16) & 0xF);
		Rn = *pRn;//getRegister((m_instr >> 16) & 0xF);
		Rd = getRegister(R0 + (m_instr >> 12) & 0xF);
		bool isBX = ((m_instr >> 8) & 0xFFFFF) == 0x12FFF;
		if (isBX)
			LOG << "DUDE! It is BX\n";
		if (type == 0xA) {
			COND(B, condition)
		}
		else if (type == 0xB) {
			COND(BL, condition)
		}
		else if (isBX) {
// 			Rn = m_registers[m_instr & 0xF];
			Rn = *getRegister(m_instr & 0xF);
			COND(BX, condition)
		}
		else if (type == 0x4 || type == 0x5 || type == 0x6 || type == 0x7)
		{
			P = (m_instr >> 24) & 1;
			U = (m_instr >> 23) & 1;
			BW = (m_instr >> 22) & 1;
			W = (m_instr >> 21) & 1;
			if (((m_instr >> 16) & 0xF) == 15)
				Rn += 8;
			if (m_instr & (1 << 25)) { // Shifted register
				LOG << "Shifted register\n";
				u32 shift = (m_instr >> 7) & 0x1F;
				u8 shiftType = (m_instr >> 5) & 3;
// 				u32 Rs = m_registers[m_instr & 0xF]; 
				u32 Rs = *getRegister(m_instr & 0xF); 
				u8 sgn;
				switch(shiftType) {
					case 0: // LSL
						Op2 = Rs << shift;
						break;
					case 1: // LSR
						Op2 = Rs >> shift;
						break;
					case 2: // ASR
						sgn = Rs & (1 << 31);
						Op2 = Rs >> shift;
						if (sgn)
							Op2 |= ((1 << shift) - 1) << (32 - shift);
						break;
					case 3: // ROR
						Op2 = Rs;
						while (shift--) {
							Op2 = (Op2 >> 1) | (Op2  << 31);
						}
						break;
				}
				if (shift)
					SET_CPSR_FLAG(C,1); // carry
			}
			else {// Shifted immediate
				Op2 = m_instr & 0xFFF;
				LOG << "Shifted immediate " << (int)Op2 << " " << (int)(Rn+Op2) << "\n";
			}
			if ((m_instr) & (1 << 20))
				COND(LDR, condition)
			else
				COND(STR, condition)
		}
		else if (type == 0x0 || type == 0x1 | type == 0x2 | type == 0x3) 
		{
			u8 opcode = (m_instr >> 21) & 0xF;
			u8 type2 = (m_instr >> 4) & 0xF;
			bool isSWP = (((m_instr >> 23) & 0xF) == 0x2) &&  (0x9 == ((m_instr >> 4) & 0xFF));
			if (isSWP){
				BW = (m_instr >> 22)&1;
				Rs = *getRegister(m_instr & 0xF);
				COND(SWP, condition)
			}
			// Multiply or Multiply long
			else if ((type == 0x0 && type2 == 0x9) ) {
				Rd = getRegister((m_instr >> 16) & 0xF);
				pOp2 = getRegister((m_instr >> 12) & 0xF);
				Op2 = *pOp2;
				Rs = *getRegister((m_instr >> 8) & 0xF);
				pRn = getRegister(m_instr & 0xF);
				Rn = *pRn;
				if (type == 0x0){
					if (opcode == 0)
						COND(MUL, condition)
					else if(opcode == 1)
						COND(MLA, condition)
					else if (opcode == 4)
						COND(UMULL, condition)
					else if (opcode == 5)
						COND(UMLAL, condition)
					else if (opcode == 6)
						COND(SMULL, condition)
				}
				
			}
			
			else if ( ((m_instr >> 7) & 1) && ((m_instr >> 4) & 1) && (type < 0x2))
			{
				P = (m_instr >> 24) & 1;
				U = (m_instr >> 23) & 1;
				W = (m_instr >> 21) & 1;
				opcode = (m_instr >> 5) & 3;
				u8 L = (m_instr >> 20) & 1;
				BW = ((opcode == 1) || (L == 1)) ? 1 : 0;
				Op2 = 0;

				if (m_instr & (1 << 22)) // Immediate offset
				{
					Op2 = (m_instr & 0xF) | ((m_instr >> 4) & 0xF0);
				}
				else
					Op2 = *getRegister(m_instr & 0xF);
				if (L || (!L && opcode == 2)){
					//TODO: This does not take into account sign extension or LDRSB
					COND(LDRHD, condition)
				}

				else
					COND(STRHD, condition)
			}

			else
			{
				if (((m_instr >> 16) & 0xF) == 15)
					Rn += 8;
				LOG << "Rn is " << Rn << "\n";
				if ((m_instr >> 25) & 1) { // Immediate as second operand
					u8 rorshift = 2 * ((m_instr >> 8) & 0xF);
					LOG << "Shift by " << (int)rorshift << "\n";
					Op2 = (m_instr & 0xFF);
					/*while (rorshift) {
						--rorshift;
						Op2 = (Op2 >> 1) | (Op2  << 31);
						Op2 = (Op2 >> 1) | (Op2  << 31);
					}
					*/
					Op2 = (Op2 >> rorshift) | (Op2 << (32 - rorshift));
					LOG << "op2 is  " << Op2 << "\n";
				}
				else { // Register as second operand
					u32 shift = 0;
	// 				u32 Rs = m_registers[(m_instr >> 8) & 0xF];
					u32 Rm = *getRegister(m_instr & 0xF);
					if ((m_instr & 0xf) == 0xf)
						Rm += 8;
					if ((m_instr >> 4) & 1) { // Shift by register
	// 					shift = m_registers[m_instr & 0xF];
						shift = *getRegister((m_instr >> 8) & 0xF);
					}
					else { // Shift by immediate
						shift = (m_instr >> 7) & 0x1F;
					}
					LOG << (int)Rm << " shift by " << (int)shift << "\n";
					u8 shiftType = (m_instr >> 5) & 3;
					u8 sgn;
					switch(shiftType) {
						// TODO: There are some special cases for LSR#0 ASR#0 and ROR#0
						case 0: // LSL
							Op2 = Rm << shift;
							break;
						case 1: // LSR
							Op2 = Rm >> shift;
							break;
						case 2: // ASR
							sgn = (Rm >> 31) & 1;
							Op2 = Rm >> shift;
							if (sgn)
								Op2 |= (((1 << shift) - 1) << (32 - shift));
							break;
						case 3: // ROR
							Op2 = Rm;
							while (shift--) {
								Op2 = (Op2 >> 1) | ((Op2 & 1)  << 31);
							}
							break;
					}
					if (shift)
						SET_CPSR_FLAG(C,1); // carry

				}	
				u8 S = (m_instr >> 20) & 1;

				if (!S && opcode >= 0x8 && opcode <= 0xb)	{
				LOG << (u32)opcode << "\n";
				Psr = (m_instr >> 22) & 1;
				Rn = m_instr & 0xF;
					u8 type = (m_instr >> 16) & 0xf;
					flags = (m_instr >> 19) & 0x1;
					control = (m_instr >> 16) & 0x1;
					if (type == 0xf){
						COND(MRS, condition)
					}
					else
						COND(MSR, condition)
				}

				else
				switch(opcode){
					case 0x0:
						COND(AND, condition)
						break;
					case 0x1:
						COND(XOR, condition)
						break;
					case 0x2:
						COND(SUB, condition)
						break;
					case 0x3:
						COND(RSB, condition)
						break;
					case 0x4:
						COND(ADD, condition)
						break;
					case 0x5:
						COND(ADC, condition)
						break;
					case 0x6:
						COND(SBC, condition)
						break;
					case 0x7:
						COND(RSC, condition)
						break;
					case 0x8:
						COND(TST, condition)
						break;
					case 0x9:
						COND(TEQ, condition)
						break;
					case 0xA:
						COND(CMP, condition)
						break;
					case 0xB:
						COND(CMN, condition)
						break;
					case 0xC:
						COND(OR, condition)
						break;
					case 0xD:
						COND(MOV, condition)
						break;
					case 0xE:
						COND(BIC, condition)
						break;
					case 0xF:
						COND(MVN, condition)
						break;
				}
			}
		}
		else if (type == 0xF){
			COND(SWI, condition)
		}
		else if (type == 0x8 || type == 0x9) {
			P     = (m_instr >> 24) & 1;
			U     = (m_instr >> 23) & 1;
			Psr   = (m_instr >> 22) & 1;
			W     = (m_instr >> 21) & 1;
// 			pRn = &m_registers[(m_instr >> 16) & 0xF];
			pRn   = getRegister((m_instr >> 16) & 0xF);
			Rlist =  m_instr & 0xFFFF;
			if (m_instr & (1 << 20))
				COND(LDM, condition)
			else
				COND(STM, condition)
		}
		else {
			LOG << "bububu " << (int)m_instr << "\n";
			m_registers[R15] += 4;
		}
		//m_jit.insert(pair<gba_addr, Instruction>(addr, instr));
		return instr;
	}
	else {
		LOG << "Unknown instruction\n";
	}
	return it->second;
}

void CPU::CheckDMA()
{
	static const gba_addr DMA_CTRL[4] 
		= {0x40000BA, 0x40000C6, 0x40000D2, 0x40000DE};
	static const gba_addr DMA_WCOUNT[4] 
		= {0x40000B8, 0x40000C4, 0x40000D0, 0x40000DC};
	static const gba_addr DMA_SAD[4]
		= {0x40000B0, 0x40000BC, 0x40000C8, 0x40000D4};
	static const gba_addr DMA_DAD[4]
		= {0x40000B4, 0x40000C0, 0x40000CC, 0x40000D8};
	for (int i = 0; i < 4; ++i) {
		u16 DMA_control = m_ioreg->Fetch16(DMA_CTRL[i]);
		if (!(DMA_control & (1 << 15)))
			continue;
		if (!(DMA_control & (1 << 9))) {
			DMA_control &= ~(1 << 15);
			m_ioreg->SetU16(DMA_control, DMA_CTRL[i]);
		}
		u32 word_count = m_ioreg->Fetch16(DMA_WCOUNT[i]);
		if (!word_count){
			if (i == 3)
				word_count = 0x10000;
			else
				word_count = 0x4000;
		}
		u32 src_addr;
		u32 dst_addr;

		src_addr = m_ioreg->Fetch32(DMA_SAD[i]);
		dst_addr = m_ioreg->Fetch32(DMA_DAD[i]);

		void *src_ptr;
		void *dst_ptr;

		u32 length = word_count;

		if (DMA_control & (1 << 10)) {
			Memory* mem = DeduceMemory(src_addr);
			src_ptr = mem->Fetch32Addr(src_addr);
			mem = DeduceMemory(dst_addr);
			dst_ptr = mem->Fetch32Addr(dst_addr);
			word_count *= 4;
		}

		else {
			Memory* mem = DeduceMemory(src_addr);
			src_ptr = mem->Fetch16Addr(src_addr);
			mem = DeduceMemory(dst_addr);
			dst_ptr = mem->Fetch16Addr(dst_addr);
			word_count *= 2;	
		}

		u8 timing = (DMA_control >> 12) & 0x3;
		
		// LOG << std::setbase(16) << m_registers[R15] << "\n";
		if (timing == 0){
			std::memcpy(dst_ptr, src_ptr, word_count);
			// LOG << std::setbase(16) << "dma " << src_addr << " " << dst_addr << " " <<  word_count << " " << i << "\n";
		}
		else {
			// LOG << std::setbase(16) << "dma " << src_addr << " " << dst_addr << " " <<  word_count << " " << i << " " <<(int) timing << "\n";
		}
	}
}
void CPU::ExportLastFrame(const char* filename) 
{ m_video->ExportLastFrame(filename);}
CPU::~CPU()
{
	SAFE_DELETE(m_bios);
	SAFE_DELETE(m_wram1);
	SAFE_DELETE(m_wram2);
	SAFE_DELETE(m_vram);
	SAFE_DELETE(m_gamepack0);
	SAFE_DELETE(m_gamepack1);
	SAFE_DELETE(m_gamepack2);
	SAFE_DELETE(m_sram);
	SAFE_DELETE(m_video);
	for (int i = 0; i < 4; ++i) {
		delete m_timers[i];
	}
}
