#include "Timer.h"
#include "Memory.h"
#include <iostream>

Timer::Timer(Memory* mem, Timer* prev, int idx) : m_prev(prev), m_internalCounter(0), m_internalTicks(0), m_modulo(0), m_reloadValue(0)
{
	m_CNTL = mem->Fetch16Addr(0x4000100 + 4 * idx);
	m_CNTH = m_CNTL + 1;
}

bool Timer::Update()
{
	u16 cnth = *m_CNTH;

	if (cnth & 0x80) {
		if (m_internalCounter >= 0xFFFF){
			m_internalCounter = m_reloadValue;
		}
		++m_internalTicks;
		if (!m_modulo) {
			switch(cnth & 0x3) {
				case 0:
					m_modulo = 1;
					break;
				case 1:
					m_modulo = 64;
					break;
				case 2:
					m_modulo = 256;
					break;
				case 3:
					m_modulo = 1024;
			}
		}
		// TODO: This is shady and possibly error prone; find a better fix?
		if (*m_CNTL != m_internalCounter) {
			m_reloadValue = *m_CNTL;
		}
		if (cnth & 0x4) {	
			if (m_prev && m_prev->m_internalCounter >= 0xFFFF)
				++m_internalCounter;
			
		}
		else {
			m_internalCounter += m_internalTicks / m_modulo;
			if (m_modulo > 1)
				m_internalTicks %= m_modulo;
			else
				m_internalTicks = 0;
		}
		*m_CNTL = m_internalCounter;
		return (cnth & 0x40) && m_internalCounter >= 0xFFFF;
		
	}
	*m_CNTL = 0;
	return false;
}