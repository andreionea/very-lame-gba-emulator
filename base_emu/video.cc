#include "video.h"
#include "Memory.h"
#include "CPU.h"
#include "log.h"
#include "interrupts.h"
#include "consts.h"

#include "background.h"
#include <vector>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <cstdio>

using std::vector;

Video::Video() : bg0(0), bg1(0), bg2(0), bg3(0), framebuffer(0), screenData(0)
{
}
void Video::Init(CPU* cpu)
{
	Memory* m;
	m = cpu->m_ioreg;
	dispcnt  = m->Fetch16Addr(0x4000000);
	dispstat = m->Fetch16Addr(0x4000004);
	vcount   = m->Fetch16Addr(0x4000006);
	bgOffsets= m->Fetch16Addr(0x4000010);

	vram	 = cpu->m_vram->Fetch16Addr(0x6000000);
	palette  = cpu->m_bg_obj->Fetch16Addr(0x05000000);
	IF       = cpu->m_IF;


	screenData = new u16[GBA_WIDTH * GBA_HEIGHT];

	*vcount = 0;
	clock_cnt = 960;

	bg0 = new Background(0, m->Fetch16Addr(0x4000008 + 2 * 0));
	bg0->Init();
	bg1 = new Background(1, m->Fetch16Addr(0x4000008 + 2 * 1));
	bg1->Init();
	bg2 = new Background(2, m->Fetch16Addr(0x4000008 + 2 * 2));
	bg2->Init();
	bg3 = new Background(3, m->Fetch16Addr(0x4000008 + 2 * 3));
	bg3->Init();
}

void Video::BGMode0()
{
	vector<Background*> v;
	u16 dcnt = *dispcnt;

	if ((dcnt >>  8) & 1)
		v.push_back(bg0);
	if ((dcnt >>  9) & 1)
		v.push_back(bg1);
	if ((dcnt >> 10) & 1)
		v.push_back(bg2);
	if ((dcnt >> 11) & 1)
		v.push_back(bg3);
	memset(screenData, 0, 2 * GBA_WIDTH * GBA_HEIGHT);

	std::sort(v.begin(), v.end(), [](const Background* a, const Background* b) 
		{
			if (*a->bgcnt > *b->bgcnt)
				return true;
			if (*a->bgcnt < *b->bgcnt)
				return false;
			return (a->id > b->id);
		});
	for (auto &i : v) 
	{
		u16 bgcnt = *i->bgcnt;
		u8 size = (bgcnt >> 15) & 3;
		switch(size){
			case 0:
				i->w = i->h = 256;
				break;
			case 1:
				i->w = 512;
				i->h = 256;
				break;
			case 2:
				i->w = 256;
				i->h = 512;
				break;
			case 3:
				i->w = 512;
				i->h = 512;
				break;
		}
		// TODO: Add code to verify if bg has been "dirtified"
		i->Update0(vram, palette, (bgcnt >> 7) & 1);
		u16 offsetX = *(bgOffsets + 2 * i->id);
		u16 offsetY = *(bgOffsets + 2 * i->id + 1);
		u16* scrPtr = screenData;
		for (u16 y = offsetY, bgY = 0; bgY < GBA_HEIGHT; ++y, ++bgY)
			for (u16 x = offsetX, bgX = 0; bgX < GBA_WIDTH; ++x, ++bgX, ++scrPtr){
				u16 col = i->data[(y % i->h) * i->w + (x % i->w)];
				if ((col & (1 << 15)) || !(*scrPtr & (1 << 15)))
					*scrPtr = col;
					//screenData[bgY * GBA_WIDTH + bgX] = col;
			}
	}

	ExportLastFrame("");

	framebuffer = screenData;

}

void Video::BGMode3()
{
	u16 dcnt = *dispcnt;
	if ((dcnt >> 10) & 1){
		bg2->Update3(vram);
		framebuffer = bg2->data;
	}
	else
		framebuffer = 0;
	ExportLastFrame("");
}

void Video::BGMode4()
{
	u16 dcnt = *dispcnt;
	if (((dcnt >> 10) & 1)){
		bg2->Update4(vram, palette, (dcnt & (1 << 4))? true:false);
		framebuffer = bg2->data;
	}
	else
		framebuffer = 0;
	ExportLastFrame("");
}

void Video::BGMode5()
{
	u16 dcnt = *dispcnt;
	if (((dcnt >> 10) & 1)){
		bg2->Update5(vram, (dcnt & (1 << 4))? true:false);
		for (u16 y = 0; y < GBA_HEIGHT; y += 2){
			u16* linedata = screenData + y * GBA_WIDTH;
			for (u16 x = 0; x < GBA_WIDTH; ++x) {
				linedata[x] = bg2->data[(y % 2) * bg2->w + x % 2];
			}
			memcpy(linedata + GBA_WIDTH, linedata, GBA_WIDTH * 2);
		}
		framebuffer = screenData;
	}
	else
		framebuffer = 0;

	ExportLastFrame("");
}

void Video::Update()
{
	vblankDone = false;
	--clock_cnt;
	u16 interrupt_flag = *IF;
	static u32 mask_disable =  ~((1 << IRQ_VBLANK) | (1 << IRQ_HBLANK) | (1 << IRQ_VCOUNT));
	// interrupt_flag &= mask_disable;
	// Reached the end of a clock counter
	if (clock_cnt <= 0) {
		// Go from H-BLANK to next line
		if ((*dispstat) & 0x2) {
			*vcount += 1;
			*dispstat &= ~0x2;
			clock_cnt = 960;
			if ((*vcount) == 160){
				vblankDone = true;
				*dispstat |= 0x1;
				// If VBLANK intr is enabled from LCD control IO reg, raise it
				if ((*dispstat) & 0x8) {
					// interrupt_flag &= mask_disable;
					interrupt_flag |= (1 << IRQ_VBLANK);
				}
				// std::cout << *dispcnt << "\n";
				mode = *dispcnt & 0x7;
				switch(mode) {
					case 0:
						BGMode0();
						break;
					case 3:
						BGMode3();
						break;
					case 4:
						BGMode4();
						break;
					case 5:
						BGMode5();
						break;
					case 1:
					case 2:
					case 6:
					case 7:
						std::cout << "Unimplemented video mode\n";
				}
			}
			else
			//  
			if ((*vcount) == 228) {
				*vcount = 0;
				*dispstat &= ~0x1;
				// TODO: this needs to be a frame blit operation
			}
			// Reached VCOUNT setting value
			if (*vcount == ((*dispstat) >> 8))
			{
				*dispstat |= 0x4;
				// If VCOUNT intr is enabled from LCD control IO reg, raise it
				if ((*dispstat) & 0x20) {
					// interrupt_flag &= mask_disable;
					interrupt_flag |= (1 << IRQ_VCOUNT);
				}
			}
		}
		else {
			clock_cnt = 272;
			*dispstat |= 0x2;

			if ((*dispstat) & 0x10) {
				// interrupt_flag &= mask_disable;
				interrupt_flag |= (1 << IRQ_HBLANK);
			}
		}
	}
	else {
		// TODO: This needs to be modified to make data transfers
	}

	*IF = interrupt_flag;
}

void Video::ExportLastFrame(const char* filename)
{
	#if defined(DEBUG)
	bg0->ExportToFile("bg0.ppm");
	bg1->ExportToFile("bg1.ppm");
	bg2->ExportToFile("bg2.ppm");
	bg3->ExportToFile("bg3.ppm");
	std::ofstream fout("palette.ppm");
	fout << "P3\n";
	fout << 16 << " " << 16 << "\n";
	fout << (int)0x1F << "\n";


	for (int i = 0; i < 16; ++i){
		for (int j = 0; j < 16; ++j){
			u16 col = palette[i * 16 + j];
			fout << (col & 0x1F) << " " << ((col >> 5) & 0x1F) 
		  << " " << ((col >> 10) & 0x1F) << " ";
			fout << "\n";
		}
	}

	fout.close();
	#endif
}

Video::~Video()
{
	delete bg0;
	delete bg1;
	delete bg2;
	delete bg3;
	delete[] screenData;
}