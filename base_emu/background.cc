#include "background.h"
#include "consts.h"
#include <iostream>
#include <fstream>
#include <cstring>

void Background::ExportToFile(const char* filename)
{
	std::ofstream fout(filename);
	fout << "P3\n";
	fout << w << " " << h << "\n";
	fout << (int)0x1F << "\n";


	for (int i = 0; i < h; ++i){
		for (int j = 0; j < w; ++j){
			fout << (data[i * w + j] & 0x1F) << " " << ((data[i * w + j] >> 5) & 0x1F) 
		  << " " << ((data[i * w + j] >> 10) & 0x1F) << " ";
			fout << "\n";
		}
	}

	fout.close();
}

void Background::Update0(u16* vram, u16* palette, bool is256)
{
	u8 charBlock = ((*bgcnt) >> 2) & 0x03;
	u8 scrnBlock = ((*bgcnt) >> 8) & 0x1F;
	u16* chrMap = vram + charBlock *  0x2000;
	u16* scrMap = vram + scrnBlock *  0x0400;
	u8 colorSize = is256 ? 64 : 32;
	for (int y = 0, blkY = 0; y < h; y += 8, blkY++) {
		for (int x = 0, blkX = 0; x < w; x += 8, blkX++){
			u16 idx;
			u16* screenMap = scrMap + blkY * w / 8 + blkX + ((w / 256) * (blkY / 32) + blkX / 32)  * 0x400;
			idx = *screenMap;
			u8* baseBlk = reinterpret_cast<u8*>(chrMap) + (idx & 0x3FF) * colorSize;
			gba_addr offset = baseBlk - (u8*)vram;
			baseBlk = (u8*)vram + offset;
			u32 palnum = ((idx >> 12) & 0xf) * 16;
			for (int i = 0; i < 8; ++i){
				for (int j = 0; j < 8; ++j) {
					if (is256) {
						u8 col = baseBlk[i * 8 + j];
						data[(y + i) * w + x + j] = palette[col];
						if (col)
							data[(y + i) * w + x + j] |= 1 << 15;
						else
							data[(y + i) * w + x + j] &= ~(1 << 15);
					}
					else {
						u8 col = baseBlk[i * 4 + j / 2];
						u8 col1 = (col & 0xF);
						u8 col2 = ((col >> 4) & 0xF);
						data[(y + i) * w + x + j] = palette[palnum + col1];
						if (col1)
							data[(y + i) * w + x + j] |= 1 << 15;
						else
							data[(y + i) * w + x + j] &= ~(1 << 15);
						j++;
						data[(y + i) * w + x + j] = palette[palnum + col2];
						if (col2)
							data[(y + i) * w + x + j] |= 1 << 15;
						else
							data[(y + i) * w + x + j] &= ~(1 << 15);
					}
				}
			}
		}
	}
}

void Background::Update3(u16* vram)
{
	w = GBA_WIDTH;
	h = GBA_HEIGHT;
	memcpy(data, vram, w * h * 2);
}
void Background::Update4(u16* vram, u16* palette, bool frame)
{
	w = GBA_WIDTH;
	h = GBA_HEIGHT;
	u8* bytes = (u8*)vram;
	if (frame)
		bytes += 0xa000;
	int k = 0;
	for (unsigned i = 0; i < h; ++i)
		for (unsigned j = 0; j < w; ++j, k++){
			data[k] = palette[bytes[k]];
		}
}
void Background::Update5(u16* vram, bool frame)
{
	w = 160;
	h = 128;
	if (frame)
		vram += 0xa000;
	memcpy(data, vram, w * h * 2);

}