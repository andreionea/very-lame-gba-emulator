#include "Memory.h"
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#ifdef __ANDROID__
#include "FileRes.h"
#include "config.h"
#endif

#include "log.h"

Memory::Memory(gba_addr base, gba_addr size)
	:base_addr(base), mem_size(size), owns_data(true)
{
	data = new u8[size];
	memset(data, 0, size);
	mask = size - 1;
	// Stupid hack
	if (mem_size == 0x18000) {
		mask = 0x1FFFF;
	}
}
Memory::Memory(Memory* mem, gba_addr base):base_addr(base), data(mem->data), mem_size(mem->mem_size), mask(mem->mask), owns_data(false)
{

}
Memory::Memory(gba_addr base, const char* filename, gba_addr max_size):base_addr(base), owns_data(true)
{
	data = 0;
	mem_size = 0;
#ifdef __ANDROID__
	auto f = FileRes::GetResource(filename);
	f->Data();
	mem_size = f->Size();
	LOGME("ROM size %d", mem_size);
	// If file about to be read exceeds the RAM size, slash it
	if (max_size && (mem_size > max_size && max_size)) {
		mem_size = max_size;
	}
	data = new u8[max_size];
	memcpy(data, f->Data(), mem_size);
	mem_size = max_size;
#else
	FILE *f = fopen(filename, "rb");
	if (!f)
		return;
	if (fseek(f, 0, SEEK_END) != 0)
		return;
	mem_size = ftell(f);
	if (!mem_size){
		fclose(f);
		return;
	}
	// If file about to be read exceeds the RAM size, slash it
	if (max_size && (mem_size > max_size && max_size)) {
		mem_size = max_size;
	}

	// LOG << std::setbase(16) << filename << " " << mem_size << "\n";

	data = new u8[max_size];
	fseek(f, 0, SEEK_SET);
	fread(data, 1, mem_size, f);
	mem_size = max_size;
	fclose(f);
#endif
	mask = mem_size - 1;
}
Memory::~Memory()
{
	if (owns_data)
		SAFE_DELETE_ARRAY(data);
}