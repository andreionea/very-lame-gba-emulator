#include "inputhandler.h"

void InputHandler::PressKey(InputKey key)
{
	std::unique_lock<std::mutex> lck(mtx);
	keys &= ~key;
}
void InputHandler::ReleaseKey(InputKey key)
{
	std::unique_lock<std::mutex> lck(mtx);
	keys |= key;
}
void InputHandler::UpdateIO()
{
	std::unique_lock<std::mutex> lck(mtx);
	*io_addr = keys;
}