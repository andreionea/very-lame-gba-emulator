#include "config.h"
#include "FileRes.h"
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/native_activity.h>
#include <unistd.h>

#include "Utils.h"

int FileRes::GetFD()
{
	if (m_type == TYPE_ASSET) {
		int ret;
		ANativeActivity* native_app = Utils::GetInstance()->GetNativeActivity();
		AAssetManager* mgr = native_app->assetManager;
		AAsset *asset = AAssetManager_open(mgr, m_filename.c_str(), AASSET_MODE_STREAMING);
		off_t a,b;
		ret = AAsset_openFileDescriptor(asset, &a, &b);
		LOGME("offset %d %d", a, b);
		AAsset_close(asset);
		return ret;
	}
	return -1;
}

void FileRes::Initialize()
{
	if (m_initialized)
		return;
	if (m_type == TYPE_ASSET){
		ANativeActivity* native_app = Utils::GetInstance()->GetNativeActivity();
		AAssetManager* mgr = native_app->assetManager;
		AAsset *asset = AAssetManager_open(mgr, m_filename.c_str(), AASSET_MODE_STREAMING);
		m_contents = new unsigned char[m_size+1];
		AAsset_read(asset, m_contents, m_size);
		AAsset_close(asset);
		m_contents[m_size] = 0;
		m_initialized = true;
	}
}
FileRes::FileRes(const std::string& str) : m_contents(0), m_initialized(false), m_type(TYPE_UNKNOWN), m_valid(false)
{
	LOGME("File %s", str.c_str());
	auto pos = str.find("asset://");
	if (pos == 0) {
		m_type = TYPE_ASSET;
		ANativeActivity* native_app = Utils::GetInstance()->GetNativeActivity();
		AAssetManager* mgr = native_app->assetManager;
		m_filename = str.substr(8);
		AAsset *asset = AAssetManager_open(mgr, m_filename.c_str(), AASSET_MODE_STREAMING);
		if (asset){
			m_size = AAsset_getLength(asset);
			AAsset_close(asset);
			m_valid = true;
		}
	}
	else {
		LOGME("position %d", pos);
	}
}

FileRes::~FileRes()
{
	delete[] m_contents;
}