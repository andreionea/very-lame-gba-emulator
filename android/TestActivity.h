#ifndef TestActivity_h
#define TestActivity_h

#include "EGLWrapper.h"
#include "android_native_app_glue.h"


class TestActivity
{
public:
	virtual ~TestActivity();
	TestActivity(struct android_app* app);
	virtual void MainLoop();
	virtual void Update();
	virtual void Draw();
	void InitializeEGL(ANativeWindow*);
	virtual void LoopEvents();
	virtual void OnPause();
	void DestroyWindow();
private:
	struct android_app* m_app;
	EGLWrapper m_egl;
};

#endif