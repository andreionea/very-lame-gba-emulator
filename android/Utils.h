#ifndef Utils_h
#define Utils_h

#include <memory>

struct ANativeActivity;
class Utils
{
public:
	static Utils* GetInstance();
	void SetNativeActivity(struct ANativeActivity* activity) { m_nativeapp = activity; }
	struct ANativeActivity* GetNativeActivity() const {return m_nativeapp; }
private:
	Utils();
	~Utils();
	struct ANativeActivity* m_nativeapp;
	static Utils* s_instance;
};

#endif