#include "config.h"
#include "ShaderProgram.h"
#include "FileRes.h"
#include <GLES2/gl2.h>
#include <string>
#include <cstring>
#include <vector>

ShaderProgram::ShaderProgram(const std::string& filename):
	 m_filename(filename)
	,m_initialized(false)
{
}

ShaderProgram::~ShaderProgram()
{

}

static GLuint createShader(const char* content, GLenum type)
{
	GLuint ret = glCreateShader(type);
	int len = strlen(content);
	// LOGME("Compiling shader:%s", content);
	// LOGME("Shader handle:%d", ret);
	// LOGME("Shader length:%d", len);
	glShaderSource(ret, 1, &content, &len);
	glCompileShader(ret);
	GLint success = 0;
	glGetShaderiv(ret, GL_COMPILE_STATUS, &success);
	if (success != GL_FALSE){
		// LOGME("Compile successful");
		return ret;
	}
	GLint maxLength = 0;
	glGetShaderiv(ret, GL_INFO_LOG_LENGTH, &maxLength);
	std::vector<char> errorLog(maxLength);
	glGetShaderInfoLog(ret, maxLength, &maxLength, &errorLog[0]);
	glDeleteShader(ret);
	// LOGME("Error length %d", maxLength);
	// LOGME("Error compiling shader:%s", &errorLog[0]);
	ret = 0;
	return ret;
}

bool ShaderProgram::Initialize()
{
	if (m_initialized)
		return true;
	m_prog = glCreateProgram();
	auto fileFS = FileRes::GetResource(m_filename + ".fs");
	auto fileVS = FileRes::GetResource(m_filename + ".vs");
	GLuint vertex_shader;
	GLuint fragment_shader;
	vertex_shader = createShader(reinterpret_cast<const char*>(fileVS->Data()), GL_VERTEX_SHADER);
	if (!vertex_shader)
		return false;
 	fragment_shader = createShader(reinterpret_cast<const char*>(fileFS->Data()), GL_FRAGMENT_SHADER);
	if (!fragment_shader) {
		glDeleteShader(vertex_shader);
		vertex_shader = 0;
		return false;
	}
	glAttachShader(m_prog, vertex_shader);
	glAttachShader(m_prog, fragment_shader);
	glLinkProgram(m_prog);
	GLint isLinked = 0;
	glGetProgramiv(m_prog, GL_LINK_STATUS, (int *)&isLinked);
	if(isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(m_prog, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(m_prog, maxLength, &maxLength, &infoLog[0]);
	 	glDeleteProgram(m_prog);
	 	m_prog = 0;
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		return false; 
	}

	m_initialized = true;
	return true;
}

void ShaderProgram::Bind()
{
	if (m_initialized){
		glUseProgram(m_prog);
	}
}