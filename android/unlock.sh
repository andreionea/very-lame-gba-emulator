#!/bin/bash

if [ "$(adb shell dumpsys power | grep mScreenOn= | grep -oE '(true|false)')" == false ] ; then
    adb shell input keyevent 26 # wakeup
    adb shell input keyevent 82 # unlock
elif [ "$(adb shell dumpsys power | grep ^Display | grep -oE '(ON|OFF)')" == OFF ] ; then
    adb shell input keyevent 26 # wakeup
    adb shell input keyevent 82 # unlock
fi