#include "config.h"
#include <android/log.h>
#include <android/window.h>
#include "TestActivity.h"

#include <CPU.h>
#include <Video.h>

#include "Utils.h"
#include "FileRes.h"
#include "ShaderProgram.h"
#include "Texture.h"
#include <consts.h>
#include <mutex>
#include <thread>
#include <vector>


std::shared_ptr<ShaderProgram> sp;
std::shared_ptr<Texture> tex;
std::mutex mtxLoop;
std::mutex mtxFB;
volatile bool done = false;
volatile bool shouldSwap = true;
volatile InputHandler* ih = 0;
std::vector<u8> frontB;
std::vector<u8> backB;
void fn_update(void);

void onAppCmd(struct android_app* app, int32_t cmd);

void android_main(struct android_app* app)
{
	app_dummy();
	TestActivity* activity = new TestActivity(app);
	ANativeActivity_setWindowFlags(app->activity, AWINDOW_FLAG_KEEP_SCREEN_ON, 0);
	app->userData = activity;
	app->onAppCmd = onAppCmd;
	Utils::GetInstance()->SetNativeActivity(app->activity);
	frontB.reserve(GBA_WIDTH * GBA_HEIGHT * 2);
	backB.reserve(GBA_WIDTH * GBA_HEIGHT * 2);
	std::thread loopThread(fn_update);
	activity->MainLoop();
	app->userData = 0;
	delete activity;
}

void TestActivity::LoopEvents()
{
	int32_t ident;
	int32_t events;
	struct android_poll_source* source;

	while ((ident = ALooper_pollAll(1, NULL, &events, (void**)&source)) >= 0)
	{
		if (source != NULL)
		{
			source->process(m_app, source);
		}
    }

}

void TestActivity::Update()
{

}
void fn_update(void)
{

	CPU gba;
	gba.Init("asset://gba_bios.bin");
	gba.loadGBA("asset://plasma.gba");
	ih = gba.GetInputHandler();
	while (true){
		std::unique_lock<std::mutex> lck(mtxLoop);
		if (done)
			break;
		gba.Step();
		if (gba.GetVideo()->VBlank() && gba.GetVideo()->GetFramebuffer()) {
				memcpy(&backB[0], gba.GetVideo()->GetFramebuffer(), GBA_WIDTH * GBA_HEIGHT * 2);
				{
					std::unique_lock<std::mutex> lck2(mtxFB);
					if (shouldSwap)
						frontB.swap(backB);
					shouldSwap = false;
				}
		}
	}
	
}
void TestActivity::Draw()
{
	if (!m_egl.isInitialized())
		return;
	{
		std::unique_lock<std::mutex> lck2(mtxFB);
		if (!shouldSwap) {
			tex->Update(&frontB[0]);
			shouldSwap = true;
		}	
	}
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	sp->Bind();
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(0, 0);
	tex->Bind();
	static const float positions[8] =
	{
		-1.0,  1.0,
		 1.0,  1.0,
		 1.0, -1.0,
		-1.0, -1.0
	};
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, &positions[0]);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	m_egl.SwapBuffers();
}
void TestActivity::MainLoop()
{
	while (true) {
		LoopEvents();
		Update();
		Draw();
	}
}


void TestActivity::InitializeEGL(ANativeWindow* window)
{
	m_egl.Initialize(window);
	sp = ShaderProgram::GetResource("asset://simple_tex");
	bool result = sp->Initialize();
	tex = Texture::GetResource("");
	tex->Initialize();
	if (!result)
		LOGME("WTF?!");
	
}

TestActivity::TestActivity(struct android_app* app): m_app(app)
{

}

TestActivity::~TestActivity()
{

}
void TestActivity::OnPause()
{
}
void TestActivity::DestroyWindow()
{
	m_egl.DestroySurface();
}
void onAppCmd(struct android_app* app, int32_t cmd)
{
	TestActivity* activity = (TestActivity*)(app->userData);
	switch(cmd) {
		case APP_CMD_INIT_WINDOW:
			LOGME("Init window");
			activity->InitializeEGL(app->window);	
			break;
		case APP_CMD_TERM_WINDOW:
			activity->DestroyWindow();
			break;
		case APP_CMD_PAUSE:
			activity->OnPause();	
			break;

	}
}