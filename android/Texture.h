#ifndef Texture_h
#define Texture_h

#include "ManagedResource.h"
#include "FileRes.h"
class Texture: public ManagedResource<Texture>
{
	DECL_MANAGED_RES(Texture);
public:
	void Initialize();
	void Bind();
	void Update(const unsigned char*);
private:
	unsigned m_handle;
	unsigned m_width, m_height;
	bool m_initialized;
};
#endif