#include "EGLWrapper.h"
#include <EGL/egl.h>
#include "config.h"

EGLWrapper::EGLWrapper():m_initialized(false), m_context(EGL_NO_CONTEXT), m_surface(EGL_NO_SURFACE), m_hasconf(false)
{
	m_display = (EGLDisplay)EGL_BAD_DISPLAY;
}

EGLWrapper::~EGLWrapper()
{
	if (m_display != (EGLDisplay)EGL_BAD_DISPLAY){
		eglTerminate(m_display);
		m_display = (EGLDisplay)EGL_BAD_DISPLAY;
	}
}

void EGLWrapper::SwapBuffers()
{
	if (m_initialized)
		eglSwapBuffers(m_display, m_surface);
}

void EGLWrapper::DestroySurface()
{
	m_initialized = false;
	if (m_surface) {
		eglDestroySurface(m_surface, m_display);
		m_surface = EGL_NO_SURFACE;
	}
	if (m_context)
		eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
}

void EGLWrapper::ChooseConfig()
{
	if (m_hasconf)
		return;
	m_hasconf = true;
	EGLint attrib_list[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_BLUE_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_RED_SIZE, 8,
		EGL_NONE
	};


	EGLint num_configs;

	eglChooseConfig(m_display, attrib_list, &m_config, 1, &num_configs);
}

void EGLWrapper::InitializeContext()
{
	if (m_context == EGL_NO_CONTEXT)
	{
		EGLint attrib_list[] = {
			EGL_CONTEXT_MAJOR_VERSION_KHR, 2,
			EGL_NONE
		};
		eglBindAPI(EGL_OPENGL_ES_API);
		m_context = eglCreateContext(m_display, m_config, EGL_NO_CONTEXT, attrib_list);
	}
	eglMakeCurrent(m_display, m_surface, m_surface, m_context);
}

void EGLWrapper::Initialize(ANativeWindow* window)
{
	if (m_initialized)
		return;
	m_window = window;
	m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	int majv, minv;
	if (m_context == EGL_NO_CONTEXT)
		eglInitialize(m_display, &majv, &minv);

	LOGME("EGL %d.%d", majv, minv);

	ChooseConfig();

	m_surface = eglCreateWindowSurface(m_display, m_config, window, NULL);

	InitializeContext();

	m_initialized = true;
}