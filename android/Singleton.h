#ifndef Singleton_h
#define Singleton_h
template<class T>
class Singleton
{
static T* s_instance;
public:
	static void CreateInstance() { if (!HasInstance()) s_instance = new T(); }
	static T* GetInstance() { return s_instance; }
	static bool HasInstance() {return s_instance; }
	static void DestroyInstance() { delete s_instance; }
};

#define DECLARE_SINGLETON(classname) \
private:\
classname();\
~classname();\ 
friend class Singleton<classname>

#define DEFINE_SINGLETON(classname) \
template<>\
classname *Singleton<classname>::s_instance = 0

#endif