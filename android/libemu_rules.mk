%.o:../base_emu/%.cc
	@$(ECHO) compiling libemu/$*.cc
	@$(CXX) $(CXXFLAGS) -I. -I../include -c $< -o $@
libemu.a: $(LIBEMU_OBJS)
	@$(ECHO) Linking $@
	@$(AR) cru $@ $^