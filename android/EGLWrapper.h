#ifndef EGLWrapper_h
#define EGLWrapper_h
#include <EGL/egl.h>

#ifndef EGL_KHR_create_context
#define EGL_KHR_create_context 1
#define EGL_CONTEXT_MAJOR_VERSION_KHR     0x3098
#define EGL_CONTEXT_MINOR_VERSION_KHR     0x30FB
#define EGL_CONTEXT_FLAGS_KHR             0x30FC
#define EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR 0x30FD
#define EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_KHR 0x31BD
#define EGL_NO_RESET_NOTIFICATION_KHR     0x31BE
#define EGL_LOSE_CONTEXT_ON_RESET_KHR     0x31BF
#define EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR  0x00000001
#define EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE_BIT_KHR 0x00000002
#define EGL_CONTEXT_OPENGL_ROBUST_ACCESS_BIT_KHR 0x00000004
#define EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR 0x00000001
#define EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT_KHR 0x00000002
#define EGL_OPENGL_ES3_BIT_KHR            0x00000040
#endif /* EGL_KHR_create_context */

class EGLWrapper {
public:
	EGLWrapper();
	~EGLWrapper();
	bool isInitialized() const {return m_initialized;}
	void Initialize(ANativeWindow*);
	void SwapBuffers();
	void DestroySurface();
private:
	void ChooseConfig();
	void InitializeContext();
	bool          m_initialized;
	EGLDisplay    m_display;
	EGLSurface    m_surface;
	EGLContext    m_context;
	EGLConfig     m_config;
	ANativeWindow *m_window;
	bool m_hasconf;
};
#endif