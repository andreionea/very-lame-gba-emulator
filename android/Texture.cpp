#include "config.h"
#include "Texture.h"

#include <cstring>
#include <GLES2/gl2.h>
#include <consts.h>

Texture::~Texture()
{
	if (m_handle)
		glDeleteTextures(1, &m_handle);
}

Texture::Texture(const std::string& filename):m_handle(0), m_width(0), m_height(0), m_initialized(false)
{
}


void Texture::Update(const unsigned char* data)
{
	glBindTexture(GL_TEXTURE_2D, m_handle);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB5_A1, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_SHORT_5_5_5_1, data);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::Initialize()
{
	if (m_initialized)
		return;
	m_width = GBA_WIDTH;
	m_height = GBA_HEIGHT;
	glGenTextures(1, &m_handle);
	LOGME("Tex handle %d", m_handle);
	glBindTexture(GL_TEXTURE_2D, m_handle);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB5_A1, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_SHORT_5_5_5_1, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	m_initialized = true;
}

void Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, m_handle);
}