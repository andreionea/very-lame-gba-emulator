#ifndef ManagedResource_h
#define ManagedResource_h

#include <memory>
#include <map>
#include <string>

template<class Resource>
class ManagedResource {
	public:
		static std::shared_ptr<Resource> GetResource(const std::string& key) {
			auto end = s_resources.end();
			auto x = s_resources.find(key);
			if (x == end || x->second.expired()) {
				auto ret = std::shared_ptr<Resource>(new Resource(key), DeleteResource);
				std::weak_ptr<Resource> weak = ret;
				s_resources[key] = weak;
				return ret;
			} 
			return x->second.lock();

		}
		virtual ~ManagedResource() {}
	private:
		static void DeleteResource(Resource* res) {delete res;}
		static std::map<std::string, std::weak_ptr<Resource>> s_resources;
};

#define DECL_MANAGED_RES(classname) \
private:\
	classname(const std::string&);\
	virtual ~classname();\
	classname(const classname &) = delete;\
	classname& operator=(const classname&) = delete;\
	friend class ManagedResource<classname>

template<typename T>
std::map<std::string, std::weak_ptr<T>> ManagedResource<T>::s_resources;

#endif