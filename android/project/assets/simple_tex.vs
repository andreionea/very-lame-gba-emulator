#version 310 es
layout (location = 0) in lowp vec2 pos;
layout (location = 0) out lowp vec2 uv;
void main()
{
	uv = (pos + 1.0) * 0.5;
	uv = vec2(uv.x, 1.0 - uv.y);
	gl_Position = vec4(pos, 0.0, 1.0);
}