#version 310 es
in lowp vec2 uv;
layout (location = 0) uniform sampler2D tex;
layout (location = 0) out lowp vec4 oColor;
void main()
{
	oColor = texture(tex, uv).bgra;
}