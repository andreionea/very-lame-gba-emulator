#ifndef FileRes_h
#define FileRes_h

#include <string>
#include "ManagedResource.h"

class FileRes: public ManagedResource<FileRes>
{
	DECL_MANAGED_RES(FileRes);
	std::string m_filename;
	unsigned char* m_contents;
	size_t m_size;
	bool m_initialized;
	bool m_valid;
	enum {
		TYPE_ASSET,
		TYPE_UNKNOWN
	} m_type;
	void Initialize();
public:
	const unsigned char* Data() {Initialize(); return m_contents;}
	size_t Size() const { return m_size;}
	int GetFD();
	bool Valid() const { return m_valid;}
};

#endif