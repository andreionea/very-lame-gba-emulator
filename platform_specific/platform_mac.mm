#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>

#include <string>

std::string GetFilename()
{

    NSOpenPanel* panel = [NSOpenPanel openPanel];
    [panel setCanChooseFiles:YES];
    [panel setCanChooseDirectories:NO];
    [panel setAllowsMultipleSelection:NO];
    [panel setAllowedFileTypes:[NSArray arrayWithObjects:@"rom", @"bin", @"gba", nil]];
    [panel setLevel:NSFloatingWindowLevel];
    std::string ret;
    if ([panel runModal] == NSOKButton) {
        NSArray* urls = [panel URLs];
        NSString* url = [[urls objectAtIndex:0] path];
        NSLog(@"%@\n", url);
        ret = std::string([url UTF8String]);
    }
    [panel close];
    return ret;
}
