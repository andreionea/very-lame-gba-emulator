#include "CPU.h"
#include "Memory.h"
#include <iostream>
#include <iomanip>
#include "var_types.h"
#include "log.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <mutex>
#include <vector>
#include <condition_variable>
#include "Video.h"
#include "SDL.h"
#include "consts.h"
#include "inputhandler.h"

#include "platform_specific/platform_functions.h"

using namespace std;

mutex mtxLoop;
mutex mtxFB;
bool done = false;
bool shouldSwap = true;
InputHandler* ih = 0;
vector<u8> frontB;
vector<u8> backB;
void fn_update(const char* file, const char* bios)
{
	CPU gba;
	SDL_Event ev;
	ev.type = SDL_USEREVENT;
	gba.Init(bios);
	gba.loadGBA(file);
	ih = gba.GetInputHandler();
	while (true){
		unique_lock<mutex> lck(mtxLoop);
		if (done)
			break;
		gba.Step();
		if (gba.GetVideo()->VBlank() && gba.GetVideo()->GetFramebuffer()) {
			ev.user.data1 = &backB[0];

			memcpy(&backB[0], gba.GetVideo()->GetFramebuffer(), GBA_WIDTH * GBA_HEIGHT * 2);
			SDL_PushEvent(&ev);
			{
			unique_lock<mutex> lck2(mtxFB);
			if (shouldSwap)
				frontB.swap(backB);
			shouldSwap = false;
			}
		}
	}
	
	bool shouldSwap = false;
}

int main(int argc, char** argv)
{

	SDL_Window *window;
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(
        "VLGE",       
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        GBA_WIDTH,                    
        GBA_HEIGHT,                    
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL      
    );
    const char* filename = argv[1];
    const char* bios_file = NULL;
    std::string open_filename = GetFilename();
    std::string open_bios = GetFilename();
    if (open_filename.length() > 1)
    	filename = open_filename.c_str();
    if (open_bios.length() > 1)
    	bios_file = open_bios.c_str();
    else
    	bios_file = NULL;
	thread loopThread(fn_update, filename, bios_file);
	SDL_Renderer *renderer = SDL_CreateRenderer(window,
                                 -1,
                                 SDL_RENDERER_SOFTWARE);
	SDL_Texture *screen_tex =  SDL_CreateTexture(renderer,
                              SDL_PIXELFORMAT_BGR555,
                               SDL_TEXTUREACCESS_STREAMING, 
                               GBA_WIDTH,
                               GBA_HEIGHT);
	SDL_Rect screen_rect;
	screen_rect.x = screen_rect.y = 0;
	screen_rect.w = GBA_WIDTH;
	screen_rect.h = GBA_HEIGHT;
	frontB.reserve(GBA_WIDTH * GBA_HEIGHT * 2);
	backB.reserve(GBA_WIDTH * GBA_HEIGHT * 2);
	bool keepGoing = true;
	while (keepGoing)
	{
		SDL_Event event;
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT)
			break;
		if (event.type == SDL_USEREVENT) {
			if(event.user.data1) {
				void* pixdata = 0;
				int pitch;
				SDL_LockTexture(screen_tex,
                    NULL,
                    &pixdata,
                    &pitch);
					memcpy(pixdata, event.user.data1, GBA_WIDTH * GBA_HEIGHT * 2);
				{
					unique_lock<mutex> lck2(mtxFB);
					shouldSwap = true;
				}
				
				bool shouldSwap = false;SDL_UnlockTexture(screen_tex);
				SDL_RenderCopy(renderer, screen_tex, NULL, NULL);
				SDL_RenderPresent(renderer);
			}
		}
		else if (event.type == SDL_KEYDOWN) {
			switch (event.key.keysym.scancode){
				case SDL_SCANCODE_RETURN:
					ih->PressKey(InputHandler::START);
					break;
				case SDL_SCANCODE_SPACE:
					ih->PressKey(InputHandler::SELECT);
					break;
				case SDL_SCANCODE_UP:
					ih->PressKey(InputHandler::UP);
					break;
				case SDL_SCANCODE_DOWN:
					ih->ReleaseKey(InputHandler::DOWN);
					break;
				case SDL_SCANCODE_A:
					ih->PressKey(InputHandler::BUTTON_A);
					break;
				case SDL_SCANCODE_RIGHT:
					ih->ReleaseKey(InputHandler::BUTTON_R);
					break;
				default:
					{
						// NOP
					}
			}
		}
		else if (event.type == SDL_KEYUP) {
			switch (event.key.keysym.scancode){
				case SDL_SCANCODE_RETURN:
					ih->ReleaseKey(InputHandler::START);
					break;
				case SDL_SCANCODE_ESCAPE:
					keepGoing = false;
					break;
				case SDL_SCANCODE_SPACE:
					ih->ReleaseKey(InputHandler::SELECT);
					break;
				case SDL_SCANCODE_UP:
					ih->ReleaseKey(InputHandler::UP);
					break;
				case SDL_SCANCODE_DOWN:
					ih->ReleaseKey(InputHandler::DOWN);
					break;
				case SDL_SCANCODE_A:
					ih->ReleaseKey(InputHandler::BUTTON_A);
					break;
				case SDL_SCANCODE_L:
					LOG.swap();
					break;
				case SDL_SCANCODE_RIGHT:
					ih->ReleaseKey(InputHandler::BUTTON_R);
					break;
				default:
					{
						// NOP
					}
			}
		}
	}

	{	// Unlock cpu loop thread
		unique_lock<mutex> lck(mtxLoop);
		done = true;
	}

	SDL_DestroyRenderer(renderer);
	SDL_Quit();
	loopThread.join();
	return 0;
}