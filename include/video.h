#ifndef video_h
#define video_h

#include "var_types.h"
class CPU;
struct Background;

class Video{
public:
	Video();
	void Init(CPU*);
	void Update();

	// TODO: Remove this later
	void ExportLastFrame(const char* filename);

	u16* GetFramebuffer() { return framebuffer; }
	bool VBlank() { return vblankDone; }
	~Video();
private:
	void BGMode0();
	void BGMode3();
	void BGMode4();
	void BGMode5();
	int  clock_cnt;
	u16* dispcnt;
	u16* dispstat;
	u16* vcount;
	u16* IF; // Interrupt request flag of CPU

	u16* bgOffsets;

	u16* vram;
	u16* palette;
	// Allocated data for blitting several backgrounds
	u16* screenData;

	// Pointer to data used for framebuffer; might be direct pointer to 
	u16* framebuffer;

	Background* bg0;
	Background* bg1;
	Background* bg2;
	Background* bg3;
	u8 mode;
	bool vblankDone;
};

#endif