#ifndef Memory_h
#define Memory_h

#include "var_types.h"
#include "log.h"
#include <iomanip>

#define MASKED_OFFSET(addr) (data + (addr & mask))

class Memory
{
private:
	gba_addr base_addr;
	gba_addr mem_size;
	gba_addr mask;
	u8 * data;
	bool owns_data;
public:
	gba_addr Size() { return mem_size; }
	Memory(Memory*, gba_addr);
	Memory(gba_addr base, gba_addr size);
	Memory(gba_addr base, const char* filename, gba_addr max_size = 0);
	u32 Fetch32(gba_addr addr) { return *reinterpret_cast<u32*>(MASKED_OFFSET(addr));}
	u16 Fetch16(gba_addr addr) { return *reinterpret_cast<u16*>(MASKED_OFFSET(addr)); } 
	u8 Fetch8(gba_addr addr) { return *(MASKED_OFFSET(addr)); } 
	u8*  Fetch8Addr(gba_addr addr) { return MASKED_OFFSET(addr); }
	u16* Fetch16Addr(gba_addr addr) { return (u16*)(MASKED_OFFSET(addr)); }
	u32* Fetch32Addr(gba_addr addr) { return reinterpret_cast<u32*>(MASKED_OFFSET(addr)); }
	void SetU32(u32 val, gba_addr addr) { *reinterpret_cast<u32*>(MASKED_OFFSET(addr)) = val; }
	void SetU16(u16 val, gba_addr addr) { *reinterpret_cast<u16*>(MASKED_OFFSET(addr)) = val; }
	void SetU8(u8 val, gba_addr addr) { *MASKED_OFFSET(addr) = val; }
	~Memory();
	friend class CPU;
};

#endif