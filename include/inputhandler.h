#ifndef inputhandler_h
#define inputhandler_h
#include "var_types.h"
#include <mutex>

class InputHandler{
public:
	InputHandler(u16* io_addr) : io_addr(io_addr), keys(0x3ff) {}
	enum InputKey{
		BUTTON_A = 1 << 0,
		BUTTON_B = 1 << 1,
		SELECT   = 1 << 2,
		START    = 1 << 3,
		RIGHT	 = 1 << 4,
		LEFT     = 1 << 5,
		UP		 = 1 << 6,
		DOWN	 = 1 << 7,
		BUTTON_R = 1 << 8,
		BUTTON_L = 1 << 9
	};
	void PressKey(InputKey);
	void ReleaseKey(InputKey);
	void UpdateIO();
private:
	u16 keys;
	u16 *io_addr;
	std::mutex mtx;
};
#endif