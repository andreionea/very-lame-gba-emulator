#ifndef CPU_h
#define CPU_h
#include <map>
#include "var_types.h"
#include "log.h"

#define DECLARE_COND(fct)\
	template<u8 cond>\
	void fct();\
	void fct(u8 cond);

#define DEFINE_COND(fct)\
	template<u8 cond>\
	void CPU::fct()\
	{\
		LOG << #fct << "\n";\
		fct(cond);\
	}

#define COND(fct, cond) \
	switch(cond){\
				case 0:\
					instr = &CPU::fct<0>;\
					break;\
				case 1:\
					instr = &CPU::fct<1>;\
					break;\
				case 2:\
					instr = &CPU::fct<2>;\
					break;\
				case 3:\
					instr = &CPU::fct<3>;\
					break;\
				case 4:\
					instr = &CPU::fct<4>;\
					break;\
				case 5:\
					instr = &CPU::fct<5>;\
					break;\
				case 6:\
					instr = &CPU::fct<6>;\
					break;\
				case 7:\
					instr = &CPU::fct<7>;\
					break;\
				case 8:\
					instr = &CPU::fct<8>;\
					break;\
				case 9:\
					instr = &CPU::fct<9>;\
					break;\
				case 10:\
					instr = &CPU::fct<10>;\
					break;\
				case 11:\
					instr = &CPU::fct<11>;\
					break;\
				case 12:\
					instr = &CPU::fct<12>;\
					break;\
				case 13:\
					instr = &CPU::fct<13>;\
					break;\
				case 14:\
					instr = &CPU::fct<14>;\
					break;\
				case 15:\
					instr = &CPU::fct<15>;\
					break;\
				default:\
					instr = &CPU::fct<15>;\
			}
using std::map;
class Memory;
class Video;
class InputHandler;
class Timer;
class CPU
{
public:
	CPU();

	~CPU();

	enum RegisterName {
		  R0
		 ,R1
		 ,R2
		 ,R3
		 ,R4
		 ,R5
		 ,R6
		 ,R7
		 ,R8
		 ,R9
		 ,R10
		 ,R11
		 ,R12
		 ,R13 // SP
		 ,R14 // LR
		 ,R15 // PC
		 ,CPSR
		 ,SPSR
		 ,R8_fiq
		 ,R9_fiq
		 ,R10_fiq
		 ,R11_fiq
		 ,R12_fiq
		 ,R13_fiq
		 ,R14_fiq
		 ,SPSR_fiq
		 ,R13_svc
		 ,R14_svc
		 ,SPSR_svc
		 ,R13_irq
		 ,R14_irq
		 ,SPSR_irq
		 ,NREGISTERS
	};

	enum Flags{
		M = 0,
		T = 5,
		F = 6,
		I = 7,
		R = 8,
		Q = 27,
		V = 28,
		C = 29,
		Z = 30,
		N = 31
	};
#ifdef __ANDROID__
	void Init(const char* bios_filename = "asset://gba_bios.bin");
#else 
	void Init(const char* bios_filename = "gba_bios.bin");
#endif
	typedef u32 RegisterSet[NREGISTERS];

	u8 FetchU8(gba_addr addr);
	u16 FetchU16(gba_addr addr);
	u32 FetchU32(gba_addr addr);

	typedef void (CPU::*Instruction)(void);
	void Step();
private:
	union{
	u32 m_instr;
	u32 m_instrTHUMB;
	};
	bool m_isARM; // or THUMB
	enum CPUMode
	{
		USER,
		FIQ,
		SUPERVISOR,
		IRQ
	};
	CPUMode m_mode;
	map<gba_addr, Instruction> m_jit;
	Memory* DeduceMemory(gba_addr addr) const;
	Instruction FetchARMInstructionIfNeeded(gba_addr);
	Instruction FetchTHUMBInstructionIfNeeded(gba_addr);
	bool checkCond(u8 cond);
	void checkArithmeticFlags();
	void checkLogicFlags();
	u32* getRegister(unsigned int);
	
	// ARM instructions
	DECLARE_COND(B);
	DECLARE_COND(BL);
	DECLARE_COND(BX);
	DECLARE_COND(STR);
	DECLARE_COND(STRHD);
	DECLARE_COND(LDR);
	DECLARE_COND(LDRHD);
	DECLARE_COND(AND);
	DECLARE_COND(XOR);
	DECLARE_COND(SUB);
	DECLARE_COND(RSB);
	DECLARE_COND(ADD);
	DECLARE_COND(ADC);
	DECLARE_COND(SBC);
	DECLARE_COND(RSC);
	DECLARE_COND(TST);
	DECLARE_COND(TEQ);
	DECLARE_COND(CMP);
	DECLARE_COND(CMN);
	DECLARE_COND(OR);
	DECLARE_COND(MOV);
	DECLARE_COND(BIC);
	DECLARE_COND(MVN);
	DECLARE_COND(SWI);
	DECLARE_COND(MRS);
	DECLARE_COND(MSR);
	DECLARE_COND(STM);
	DECLARE_COND(LDM);
	DECLARE_COND(MUL);
	DECLARE_COND(MLA);
	DECLARE_COND(UMULL);
	DECLARE_COND(SMULL);
	DECLARE_COND(UMLAL);
	DECLARE_COND(SWP);

	//Thumb instructions
	void T_LSL();
	void T_LSR();
	void T_ASR();
	void T_ADD();
	void T_SUB();
	void T_MOV();
	void T_CMP();

	void T_AND();
	void T_EOR();
	void T_ADC();
	void T_SBC();
	void T_ROR();
	void T_TST();
	void T_NEG();
	void T_CMN();
	void T_ORR();
	void T_MUL();
	void T_BIC();
	void T_MVN();
	
	void T_LDR();
	void T_STR();

	void T_B();
	void T_BX();
	void T_BL();
	void T_BEQ();
    void T_BNE();
    void T_BCS();
    void T_BCC();
    void T_BMI();
    void T_BPL();
    void T_BVS();
    void T_BVC();
    void T_BHI();
    void T_BLS();
    void T_BGE();
    void T_BLT();
    void T_BGT();
    void T_BLE();
    void T_SWI();

    void T_PUSH();
    void T_POP();

    void T_LDMIA();
    void T_STMIA();

    void CheckInterrupts();
    void CheckTimers();

    void SwitchMode(CPUMode);
    void CheckDMA();
    void DataProcessUseR15();

	Memory* m_bios;
	Memory* m_wram1;
	Memory* m_wram2;

	Memory* m_ioreg;

	Memory* m_bg_obj;
	Memory* m_vram;
	Memory* m_oam_obj;


	Memory* m_gamepack0;
	Memory* m_gamepack1;
	Memory* m_gamepack2;
	Memory* m_sram;

	Video* m_video;
	InputHandler* m_input;

	Timer* m_timers[4];

	u8 *m_haltCNT; 

	u16 *m_IME; // Interrupt Master Enable Register
	u16 *m_IE;  // Interrupt Enable Register
	u16 *m_IF;  // Interrupt Request Flag Register

public:
	void ExportLastFrame(const char* filename);
	void loadGBA(const char*);
	Video* GetVideo() {return m_video;}
	InputHandler* GetInputHandler() { return m_input; }
	union{
		RegisterSet m_registers;
		struct {
		 u32 rRegs[16];
		 union {
		 	u32 rCPSR;
		 	struct {
				u8  N        : 1;// = 31
				u8  Z        : 1;// = 30,
				u8  C        : 1;// = 29,
				u8  V        : 1;// = 28,
				u8  Q        : 1;// = 27,
				u32 Reserved : 19;// = 8,
				u8  I 	     : 1;// = 7,
				u8  F 	     : 1;// = 6,
				u8  T 	     : 1;// = 5,
		 		u8  M 	     : 5;// = 0,
		 	}statusRegs;
		 };
		 u32 other[12];
		} m_regs;
	};
	friend class Video;
private:
	u32 Rn, Rs, Op2, Rlist;
	u32 *Rd, *pRn, *pOp2;
	u8 P, U, BW, W, Psr, flags, control;
	u32 rez;
	u8 offset;
	bool modify;
};
#endif