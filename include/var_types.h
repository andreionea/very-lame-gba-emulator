#ifndef var_types_h
#define var_types_h

typedef unsigned int gba_addr;
typedef unsigned int u32;
typedef unsigned char u8;
typedef unsigned short u16;

static_assert(sizeof(u32) == 4, "u32 must have 4 bytes");
static_assert(sizeof(u16) == 2, "u16 must have 2 bytes");

#define SWITCH_ENDIANNESS32(x) ((((x) & 0xFF) << 24) | (((x) & 0xFF00) << 8) | (((x) & 0xFF0000) >> 8) | (((x) & 0xFF000000) >> 24))
#define SAFE_DELETE(ptr) if (ptr) delete ptr
#define SAFE_DELETE_ARRAY(ptr) if (ptr) delete[] ptr; ptr = 0

#endif