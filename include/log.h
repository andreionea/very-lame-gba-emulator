#ifndef log_h
#define log_h

#include <iostream>
#include <cmath>
// #define LOG std::cout
#define LOG null_log


struct NullLog {
	NullLog():count(0){}
	void flush();
	void enable() {++count;}
	void disable() {--count; count = std::max(0, count);}
	void swap() {if (count) count = 0; else count++;}
	int count;
};

template<typename T>
NullLog& operator << (NullLog& log, const T& data)
{
	if (log.count)
		std::cout << data;
	return log;
}

template<class T>
NullLog& operator << (NullLog& log, const T* data)
{
	if (log.count)
		std::cout << data;
	return log;
}

extern NullLog null_log;
#endif