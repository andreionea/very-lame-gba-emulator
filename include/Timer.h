#ifndef Timer_h
#define Timer_h
#include "var_types.h"
class Memory;

class Timer{
public:
	Timer(Memory*, Timer*, int idx);
	bool Update();
private:
	Timer* m_prev;
	u32 m_internalCounter;
	u32 m_internalTicks;
	u16 m_modulo;
	u16 m_reloadValue;
	u16* m_CNTL;
	u16* m_CNTH;
};

#endif