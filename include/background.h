#ifndef background_h
#define background_h

#include "var_types.h"
#include <iostream> 
struct Background
{
	unsigned w, h;
	char id;
	bool dirty;
	u16* data;
	
	u16* bgcnt;
	void Init() { data = new u16[512 * 512]; }
	Background(char id, u16* bgcnt) : data(0), dirty(true), id(id), bgcnt(bgcnt), w(0), h(0) {}
	~Background() { delete[] data; }
	void Update0(u16* vram, u16* palette, bool is256);
	void Update3(u16* vram);
	void Update4(u16* vram, u16* palette, bool frame); // false = frame 0; true = frame 1
	void Update5(u16* vram, bool frame);
	void ExportToFile(const char* filename);
};

#endif